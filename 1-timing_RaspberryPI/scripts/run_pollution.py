
# -------------------------------------------------------------------------
# Run sampling data on the pi (basic block) with a given pollution factor
# -------------------------------------------------------------------------

import re
import subprocess
import sys
import os

assert len(sys.argv) == 4, "Usage: run_pollution.py file.exe pollution_factor nb_iterations\n"

fname = sys.argv[1]
pollution_factor = sys.argv[2]
nb_iterations = sys.argv[3]

# Get size and start address of section named "toto" using objdump
output = subprocess.check_output(['objdump','-h',fname])
size = 0
for line in output.splitlines():
    m = re.search('[0-9]* toto [ ]* ([0-9a-f]*)',line)
    if m:
        size = int(m.group(1),16)

# Calculate real cache pollution
print("Size : ",size)
pollution = float(pollution_factor) * float(size)
print("Pollution ", pollution)

# Execute the program
prefix = os.path.splitext(fname)[0]
output_file = prefix + "-" + str(pollution_factor) + ".txt"
print("Output file: ",output_file)
subprocess.call(["sudo","dmesg","-c"],stdout=None)
arg1 = "timingml_name=" + fname
arg2 = "dcache_pollution="+str(int(pollution))
arg3 = "nb_iteration="+str(int(nb_iterations))
arg4 = "size_in_bytes="+str(int(size))
subprocess.call(["sudo","insmod",fname,arg1,arg2,arg3,arg4])
subprocess.call(["sudo","rmmod",fname])
f=open(output_file,"w")
subprocess.call(["sudo","dmesg","-c"],stdout=f,stderr=None)
