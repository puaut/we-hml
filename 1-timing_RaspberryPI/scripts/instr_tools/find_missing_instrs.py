import json
import os
import sys

assert len(sys.argv) == 3, "Usage: script.py folder_path ref.json"

source_path = os.path.abspath(sys.argv[1])
print("Source path: ", source_path)

ref_path = sys.argv[2]

instrs_set = set()
with open(ref_path, "r") as file:
    for key in json.load(file):
        instrs_set.add(key)

covered = 0
not_covered = 0

missing_instrs = set()
for path, subdirs, files in os.walk(source_path):
    for file in files:
        file_path = os.path.join(source_path, path, file)
        filename, file_extension = os.path.splitext(file_path)
        if file_extension == ".json":
            print(file_path + " Missing instrs:")
            with open(file_path, "r") as file:
                instrs = json.load(file)
                for instr in instrs:
                    count = instrs[instr]
                    if instr not in instrs_set:
                        print("\t" + instr)
                        missing_instrs.add(instr)
                        not_covered += count
                    else:
                        covered += count

print("Summary:")
for instr in sorted(missing_instrs):
    print("\t" + instr)

print("")
print("Coverage: {}%".format(100 * covered / (covered + not_covered)))
