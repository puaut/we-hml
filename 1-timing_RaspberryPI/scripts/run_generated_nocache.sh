#!/usr/bin/env bash

# -------------------------------------------------------------------
# Run sampling data (basic blocks) several times, emptying the cache
# every time
# ------------------------------------------------------------------

#sudo insmod timing_ml_enable_perf_counters_module.ko

for file in *.ko; do
    sudo dmesg -c
    filebase=${file%.*}
    echo $file
    for i in $(seq 1 250); do  
	     sudo insmod $file timingml_name=${file} dcache_pollution=0 nb_iteration=1
	     sudo rmmod $file
    done
    sudo dmesg -c > ${filebase}-nc.txt
done
