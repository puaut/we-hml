#!/usr/bin/env bash


#sudo insmod /home/pi/timing_ml_enable_perf_counters_module.ko
#\rm *.txt

#list="0 1 2 4 8 16 32 64 128 256 512"
list="0 16 512"

for file in *.ko; do
    echo $file
    for pollution_factor in $list; do
	echo $pollution_factor
	python run_pollution.py $file $pollution_factor 2060
    done
done
