#!/usr/bin/env bash

set -e
set -a

GREEN='\e[1m\e[32m'
RED='\e[1m\e[31m'
DEFAULT='\e[0m\e[39m'

if [[ "$#" -ne 4 ]]; then
    echo "Usage: compile.sh TEMPLATE GENERATED_PATH CROSS_COMPILE INCLUDE_DIR"
    exit
fi

TEMPLATE=$1
GENERATED_PATH=$2
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CROSS_COMPILE=$3
INCLUDE_DIR=$4

echo "Template: $TEMPLATE"
echo "Generated path: $GENERATED_PATH"
echo "Cross compile: $CROSS_COMPILE"

WARNINGS="-Wno-unused-variable -Wno-declaration-after-statement -Wno-parentheses -Wno-sequence-point -Wno-overflow"

START_TIME=`date +%s`
FILES=(${GENERATED_PATH}/*_def.h)
NUM_FILES=${#FILES[@]}
echo "$NUM_FILES files"

# replace /dev/shm by something else, Linux specific
# echo 0 > /dev/shm/timingml
echo 0 > /tmp/timingml

cd ${GENERATED_PATH}

work () {
    FILENAME=$1
    # /home/bscnd/Projets/PACAP/timingml/trunk/scripts/msp430_cross_compile/generated/generated_0
    BASE=${FILENAME::-6}
    # generated_0
    NAME=$(basename ${BASE})
    echo -e "${GREEN}Compiling ${BASE}${DEFAULT}"
    mkdir ${BASE}

    sed -e "s:GENERATED_DEFINITIONS:${BASE}_def.h:g" ${TEMPLATE} > ${BASE}.c
    # sed -i -e "s:GENERATED_INIT:${BASE}_init.h:g" ${BASE}.c
    # sed -i -e "s:GENERATED_RANDINIT_REUSE:${BASE}_randinit_reuse.h:g" ${BASE}.c
    sed -i -e "s:GENERATED_CODE:${BASE}_code.h:g" ${BASE}.c

    # Compile for execution in user mode using cross compiler
    cp ${BASE}.c ${BASE}/${NAME}.c
    cd ${BASE}
    Makefile=${BASE}/Makefile
    # echo "DIR : $DIR"
    # echo "CROSS COMPILE : $CROSS_COMPILE"
    sed -e "s:XXFILE:${NAME}:g" ${DIR}/user_module_makefile > ${Makefile}
    sed -i -e "s:XXCROSS_COMPILE:${CROSS_COMPILE}:g" ${Makefile}
    sed -i -e "s:XXINCLUDE_DIR:${INCLUDE_DIR}:g" ${Makefile}
    sed -i -e "s:WARNINGS:${WARNINGS}:g" ${Makefile}
    # echo  "MAKEFILE : $Makefile"
    (make -f ${Makefile} > ${BASE}.log) || (exit 1)
    # debug
    cp *.exe ..
    # cp *.ko ..
    cd ..

    #echo $(($(</dev/shm/timingml)+1)) > /dev/shm/timingml;
    echo $(($(</tmp/timingml)+1)) > /tmp/timingml;
    #INDEX=$(</dev/shm/timingml)
    INDEX=$(</tmp/timingml)
    CURRENT_TIME=`date +%s`
    SECONDS=$((CURRENT_TIME - START_TIME))
    ETA=$(echo "scale=0; $NUM_FILES * $SECONDS / $INDEX - $SECONDS" | bc -l)
    echo -e "${RED} ${INDEX}/${NUM_FILES} Elapsed: ${SECONDS}s ETA: ${ETA}s      ${DEFAULT}"
}


#parallel work ::: ${FILES[@]}
for file in ${FILES[@]}; do
    (work $file) || (echo "Compilation failed for file $NAME" && rm $file && continue)
done

#rm /dev/shm/timingml
rm /tmp/timingml
