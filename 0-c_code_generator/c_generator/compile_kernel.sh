#!/usr/bin/env bash

set -e
set -a

GREEN='\e[1m\e[32m'
RED='\e[1m\e[31m'
DEFAULT='\e[0m\e[39m'

if [[ "$#" -ne 4 ]]; then
    echo "Usage: compile.sh TEMPLATE GENERATED_PATH LOOP_ITERATIONS CROSS_COMPILE"
    exit
fi

TEMPLATE=$1
GENERATED_PATH=$2
LOOP_ITERATIONS=$3
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CROSS_COMPILE="/bin/arm-linux-gnueabihf-"

echo "Template: $TEMPLATE"
echo "Generated path: $GENERATED_PATH"
echo "Number of loop iterations: $LOOP_ITERATIONS"
echo "Cross compile: $CROSS_COMPILE"

WARNINGS="-Wno-unused-variable -Wno-declaration-after-statement -Wno-parentheses -Wno-sequence-point -Wno-overflow"

START_TIME=`date +%s`
FILES=(${GENERATED_PATH}/*_def.h)
NUM_FILES=${#FILES[@]}
echo "$NUM_FILES files"

# replace /dev/shm by something else, Linux specific
# echo 0 > /dev/shm/timingml
echo 0 > /tmp/timingml

cd ${GENERATED_PATH}

work () {
    FILENAME=$1
    BASE=${FILENAME::-6}
    NAME=$(basename ${BASE})
    echo -e "${GREEN}Compiling ${BASE}${DEFAULT}"
    mkdir ${BASE}

    # Generate source file
    sed -e "s:GENERATED_DEFINITIONS:${BASE}_def.h:g" ${TEMPLATE} > ${BASE}.c
    sed -i -e "s:GENERATED_INIT:${BASE}_init.h:g" ${BASE}.c
    sed -i -e "s:GENERATED_CODE:${BASE}_code.h:g" ${BASE}.c
    sed -i -e "s:LOOP_ITERATIONS:${LOOP_ITERATIONS}:g" ${BASE}.c

    # Compile for execution in kernel mode using cross compiler
    cp ${BASE}.c ${BASE}/${NAME}.c
    cd ${BASE}
    Makefile=${BASE}/Makefile
    sed -e "s:XXFILE:${NAME}:g" ${DIR}/../templates/kernel_module_makefile > ${Makefile}
    sed -i -e "s:XXCROSS_COMPILE:${CROSS_COMPILE}:g" ${Makefile}
    sed -i -e "s:WARNINGS:${WARNINGS}:g" ${Makefile}
    # cat $Makefile
    (make -f ${Makefile} > ${BASE}.log) || (exit 1)
    cp *.ko ..
    cd ..

    #echo $(($(</dev/shm/timingml)+1)) > /dev/shm/timingml;
    echo $(($(</tmp/timingml)+1)) > /tmp/timingml;
    #INDEX=$(</dev/shm/timingml)
    INDEX=$(</tmp/timingml)
    CURRENT_TIME=`date +%s`
    SECONDS=$((CURRENT_TIME - START_TIME))
    ETA=$(echo "scale=0; $NUM_FILES * $SECONDS / $INDEX - $SECONDS" | bc -l)
    echo -e "${RED} ${INDEX}/${NUM_FILES} Elapsed: ${SECONDS}s ETA: ${ETA}s      ${DEFAULT}"
}


#parallel work ::: ${FILES[@]}
for file in ${FILES[@]}; do
    #echo "Compiling file "
    #echo $file
    work $file
done

#rm /dev/shm/timingml
rm /tmp/timingml
