Makefile		Compile the code

code_gen.cpp		Main code generation files
code_gen.h
json.h
main.cpp

compile_kernel.sh	Scripts to compile de code in kernel and user mode
compile_user.sh

test.json		Code generation parameters
test.json.float		Old code generation parameter (with floats)
