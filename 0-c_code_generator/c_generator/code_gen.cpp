#include "code_gen.h"
#include <vector>
#include <sstream>
#include <iostream>

#define LOG 0
#define SECTION_ATTRIBUTE "__attribute__ ((section (\"toto\"))) "

// Random float between 0 and 1
inline float frand()
{
    return static_cast<float>(rand() / (RAND_MAX + 1.));
}

// Normalize a vector of probabilities
inline void normalize(std::vector<double>& probabilities)
{
    double sum = 0;
    for (auto& proba : probabilities)
    {
        sum += proba;
    }
    for (auto& proba : probabilities)
    {
        proba /= sum;
    }
}

// Get the value of an enum from a string
template<typename Enum>
Enum get_enum_from_string(const std::string& string);

// Random generator
template<typename Enum>
class Choices
{
public:
    /**
     * @param object                Json: list of [C++ identifier, name, probability]
     * @param relative_variation    How much each probability can vary relative to itself
     * @param absolute_variation    How much each probability can vary. Squared to have a semi-normal distribution
     */
    Choices(const json& object, float relative_variation, float absolute_variation)
    {
        for (auto& val : object)
        {
            choices.push_back(val[0].get<std::string>());

            auto type = val[1].get<std::string>();
            types.push_back(get_enum_from_string<Enum>(type));

            probabilities.push_back(val[2].get<double>());
        }
        normalize(probabilities);
        for (auto& proba : probabilities)
        {
            proba += proba * relative_variation * frand();
        }
        normalize(probabilities);
        for (auto& proba : probabilities)
        {
            double r = absolute_variation * frand();
            proba += r * r;
        }
        normalize(probabilities);
    }

    /**
     * Pick a random identifier & its type
     * @param out_op    The identifier picked
     * @return  its type
     */
    Enum pick(std::string& out_op) const
    {
        double random_float = frand();
        if(random_float > 1)
        {
            random_float = 1;
        }
        double sum = 0;
        for (int index = 0; index < choices.size(); index++)
        {
            sum += probabilities[index];
            if(sum >= random_float)
            {
                out_op = choices[index];
                return types[index];
            }
        }
        assert(random_float > 0.999);
        int index = types.size() - 1;
        out_op = choices[index];
        return types[index];
    }

    /**
     * Pick a random type
     */
    Enum pick() const
    {
        std::string dummy;
        return pick(dummy);
    }

    /**
     * Pick a random identifier
     */
    std::string pick_string() const
    {
        std::string result;
        pick(result);
        return result;
    }

private:
    std::vector<std::string> choices;
    std::vector<Enum> types;
    std::vector<double> probabilities;
};

/**
 * Dummy enum
 */
enum class Default
{
    Default
};

template<>
Default get_enum_from_string<Default>(const std::string& string)
{
    return Default::Default;
}

/**
 * Operations
 */
enum class OpType
{
    Left,
    FloatLeft,
    Binary,
    FloatBinary,
    FloatAssignment,
    ConstantBinary,
    InplaceLeft,
    InplaceBinary,
    Ternary,
    StructureAssignment // Ti generate ldm instruction
};

template<>
OpType get_enum_from_string<OpType>(const std::string& string)
{
    if(string == "l")
    {
        return OpType::Left;
    }
    if (string == "fl")
    {
        return OpType::FloatLeft;
    }
    if (string == "fa")
    {
        return OpType::FloatAssignment;
    }
    else if(string == "b")
    {
        return OpType::Binary;
    }
    else if(string == "fb")
    {
        return OpType::FloatBinary;
    }
    else if(string == "cb")
    {
        return OpType::ConstantBinary;
    }
    else if(string == "il")
    {
        return OpType::InplaceLeft;
    }
    else if(string == "t")
    {
        return OpType::Ternary;
    }
    else if(string == "sa")
    {
        return OpType::StructureAssignment;
    }
    {
        assert(string == "ib");
        return OpType::InplaceBinary;
    }
}

/**
 * Declarations
 */
enum class DeclType
{
    NormalDecl,
    FloatDecl,
    ArrayDecl
};

template<>
DeclType get_enum_from_string<DeclType>(const std::string& string)
{
    if(string == "n")
    {
      return DeclType::NormalDecl;
    }
    else if (string == "f") {
      return DeclType::FloatDecl;
    }
    else
    {
      assert(string == "a");
      return DeclType::ArrayDecl;
    }
}

/**
 * Accesses
 */
enum class AccessType
{
    Normal,
    Immut,
    Array,
    ArrayImmut
};

template<>
AccessType get_enum_from_string<AccessType>(const std::string& string)
{
    if(string == "normal")
    {
        return AccessType::Normal;
    }
    else if(string == "immut")
    {
        return AccessType::Immut;
    }
    else if(string == "array")
    {
        return AccessType::Array;
    }
    else
    {
        assert(string == "array_immut");
        return AccessType::ArrayImmut;
    }
}

/**
 * Struct holding info about a variable
 */
struct Variable
{
  std::string type;
  std::string name;
  bool is_FP = false;
  bool is_array = false;
};

/**
 * Create a variable name
 * @param index The index of this variable (eg 18 for var_18)
 * @param array If this variable is an array
 * @return the name
 */
inline std::string get_var_name(int index, bool is_array)
{
    std::ostringstream var_name;
    if(is_array)
    {
        var_name << "array_";
    }
    else 
    {
        var_name << "var_";
    }
    var_name << index;
    return var_name.str();
}

/**
 * Get a random variable in variables
 * @param variables Array to pick in
 * @param array     pick array variables or normal ones?
 * @return A variable name - ignoring floats
 */
inline std::string get_rand_variable(const std::vector<Variable>& variables, bool is_array)
{
    int index;
    do
    {
        index = rand() % variables.size();
        //std::clog  <<" Mouchkil getrandvariable Creating file #" << variables.size()  << std::endl;
        //std::clog  << " is_array #" << is_array << std::endl;
       // std::clog <<  " is_FP #" << variables[index].is_FP << std::endl;
       // std::clog  << "variables is array #" << variables[index].is_array << std::endl;
      //  std::clog  << "variables index #" << variables[index]  << std::endl;



    }
    while (variables[index].is_array != is_array || variables[index].is_FP);
    //std::clog << "Non FP variable " << variables[index].name << std::endl;
    return variables[index].name;
}

inline std::string get_rand_any_variable(const std::vector<Variable>& variables)
{
    int index;
    do
    {
        index = rand() % variables.size();
        //std::clog << "Mouchkil getrandanyvariable Creating file #" << index  << std::endl;

    }
    while (variables[index].is_array 
	   || variables[index].type=="float" || variables[index].type=="double");
    //std::clog << "Non FP non short variable " << variables[index].name << " : " << variables[index].type << std::endl;
    return variables[index].name;
}

/**
 * Get a random variable in variables
 * @param variables Array to pick in
 * @param array     pick array variables or normal ones?
 * @return A variable name - ignoring floats
 */
inline std::string get_rand_float_variable(const std::vector<Variable>& variables)
{
    int index;
    do
    {
        index = rand() % variables.size();
    }
    while (variables[index].is_FP == false);
    return variables[index].name;
}

/**
 * Helper to create an array access
 * @param variables Variables to pick the array variable in
 * @param index     Index to access the array at
 * @return array_variable[index]
 */
template<typename T>
inline std::string get_rand_array_access(const std::vector<Variable>& variables, T index)
{
    std::ostringstream var_name;
    var_name << get_rand_variable(variables, true);
    var_name << "[" << index << "]";
    return var_name.str();
}

/**
 * Get a random target (ie target = smthg;)
 * @param variables     Variables to pick in
 * @param access_type   The type of access: immutable, array...
 * @return The variable name
 */
inline std::string get_rand_target(const std::vector<Variable>& variables, AccessType access_type)
{
    switch (access_type)
    {
        case AccessType::Normal:
        case AccessType::Immut: // Can't have immut there
            return get_rand_variable(variables, false);
        case AccessType::Array:
            return get_rand_array_access(variables, "array_index");
        case AccessType::ArrayImmut:
            return get_rand_array_access(variables, rand() % 256);
        default:
            assert(false);
            return {};
    }
}

/**
 * Get random operand: target = operandA * operandB
 * @param variables     Variables to pick in
 * @param access_type   Type of access: array, variable...
 * @return The variable name
 */
inline std::string get_rand_operand(const std::vector<Variable>& variables, AccessType access_type)
{
    switch (access_type)
    {
        case AccessType::Normal:
            return get_rand_variable(variables, false);
        case AccessType::Immut:
            return std::to_string(rand() % 0xFFFF); // Reduce overflows
        case AccessType::Array:
            return get_rand_array_access(variables, "array_index");
        case AccessType::ArrayImmut:
            return get_rand_array_access(variables, rand() % 256);
        default:
            assert(false);
            return {};
    }
}

/**
 * Get random operand: target = operandA * operandB
 * @param variables     Variables to pick in
 * @param access_type   Type of access: array, variable...
 * @return The variable name
 */
inline std::string get_rand_float_operand(const std::vector<Variable>& variables)
{
  static int type_float=0;
  if (type_float%2) return get_rand_float_variable(variables);
  else return std::to_string(rand() * 0.5); 
  type_float += rand();
}

/**
 * Get a random constant operand. Can either be the name of a constant static, or an integer
 * @param access_type The type of access of the variable
 * @return the variable name
 */
inline std::string get_rand_const_operand(AccessType access_type)
{
    switch (access_type)
    {
        case AccessType::Normal:
        case AccessType::Array:
            return "small_int";
        case AccessType::Immut:
        case AccessType::ArrayImmut:
            return std::to_string(1 + rand() % 7);
        default:
            assert(false);
            return {};
    }
}

/**
 * Create the file
 * @param num_variables     Number of variables
 * @param num_op            Number of operations
 * @param config            The json to use
 * @param definitions       where to write the definitions (int x; etc)
 * @param code              The actual code run for the benchmark
 * @param kernel_mode       True if code for integration as kernel module has to be generated
 */
void create_file(
        int num_variables,
        int num_op,
        const json& config,
        std::ofstream& definitions,
        std::ofstream& code)
{
    const float relative_variation = config["relative_variation"].get<float>();
    const float absolute_variation = config["absolute_variation"].get<float>();
    const Choices<OpType> operations(config["operations"], relative_variation, absolute_variation);
    const Choices<DeclType> types(config["types"], relative_variation, absolute_variation);
    const Choices<AccessType> access_type(config["access_type"], relative_variation, absolute_variation);
    const Choices<Default> if_conditions(config["if_conditions"], relative_variation, absolute_variation);

    const float if_per_line = config["if_per_line"].get<float>();
    const int if_min_lines = config["if_min_lines"].get<int>();
    const int if_max_lines = config["if_max_lines"].get<int>();

    /**
     * Define a bunch of constants for ifs (else they are optimized away, even in O0)
     */
    definitions << "static int " << SECTION_ATTRIBUTE << "zero = 0;\n";
    //code << "zero = 0;\n";

    definitions << "static int " << SECTION_ATTRIBUTE << "one = 1;\n";
    //code << "one = 1;\n";
    
    definitions << "static unsigned int " << SECTION_ATTRIBUTE << "uzero = 0;\n";
    //code << "uzero = 0;\n";

    definitions << "static unsigned int " << SECTION_ATTRIBUTE << "uone = 1;\n";
    //code << "uone = 1;\n";

    definitions << "static char " << SECTION_ATTRIBUTE << "czero=0;\n";
    //code << "czero = 0;\n";

    definitions << "static char " << SECTION_ATTRIBUTE << "cone=1;\n";
    //code << "cone = 1;\n";

    // Static array index
    int idx = rand() % 256;
    definitions << "static int " << SECTION_ATTRIBUTE << "array_index = " << idx << ";\n";

    // Static int used in bit shifts/divisions
    int value =  1 + (rand() % 7);
    definitions << "static int " << SECTION_ATTRIBUTE << "small_int = " << value << ";\n";

    // Variable containing -2 (to generate cmn)
    definitions << "static int " << SECTION_ATTRIBUTE << "minus2 = -2;\n";
    
    // Used to iterate over arrays
    // Was used to init arrays only, removed
    // FIXME: have to introduce loops with arrays in code
    // definitions << "static int tmp_index;\n";

    // Floating point variables
#ifdef GENERATE_FLOATS
    definitions << "static float fzero;\n";
    code << "fzero = 0.0;\n";

    definitions << "static float fone;\n";
    code << "fone = 1.0;\n";
    
    definitions << "static double dzero;\n";
    code << "dzero = 0.0;\n";

    definitions << "static double done;\n";
    code << "done = 1.0;\n";
#endif

    // Create one variable of type structure (for the generation of ldm instructions)
    definitions << "struct {unsigned long l,r;} my_struct1, my_struct2;" << std::endl;

    /**
     * Create the variables
     */
    std::vector<Variable> variables;

    // Make sure at least one float and double
#ifdef GENERATE_FLOATS
    Variable vf;
    vf.type = "float";
    vf.name = "fzero";
    vf.is_FP = true;
    vf.is_array = false;
    variables.push_back(vf);
    Variable vd;
    vd.type = "double";
    vd.name = "dzero";
    vd.is_FP = true;
    vd.is_array = false;
    variables.push_back(vd);
#endif
    for (int index = 0; index < num_variables; index++)
    {
#if LOG
        if(index & 0x1000)
        {
            std::clog << "Variable " << index + 1 << "/" << num_variables << "\r";
        }
#endif
        std::string type;
        DeclType declType = types.pick(type);

        // Always have at least one array and one variable:
	// First variable will always be array, and second one normal
        bool is_array = (declType == DeclType::ArrayDecl || index == 0) && index != 1;
        auto name = get_var_name(index, is_array);
        //auto name_randinit = name + "_randinit";

	Variable v;
	if (is_array) {
	  do {
	    declType = types.pick(type);
	  } while (type=="float" || type=="double");
	}
	v.type = type;
	v.name = name;
	if (type == "float" || type == "double") v.is_FP = true; else v.is_FP = false;
	v.is_array = is_array;

	variables.push_back(v);

        definitions << "static " << type << " " << SECTION_ATTRIBUTE << name << (is_array ? "[256];" : ";") << std::endl;

    }
#if LOG
    std::clog << "Variable " << num_variables << "/" << num_variables << std::endl;
#endif

    /**
     * Generate the code
     */
     // Number of lines this if scope still need to go
    int num_lines_until_end_if = 0;
    for (int index = 0; index < num_op; index++)
    {
        /**
         * Ifs
         */
        if(num_lines_until_end_if > 0)
        {
            num_lines_until_end_if--;
            if(num_lines_until_end_if == 0)
            {
                code << "}\n";
            }
        }
        if(num_lines_until_end_if == 0 && frand() < if_per_line)
        {
            num_lines_until_end_if = if_min_lines + rand() % (if_max_lines - if_min_lines);
            code << "if (" << if_conditions.pick_string() << ") {\n";
        }
#if LOG
        if(index & 0x1000)
        {
            std::clog << "Instruction " << index + 1 << "/" << num_op << "\r";
        }
#endif

        /**
         * Other op
         */
        std::string op;
        OpType opType = operations.pick(op);

        auto target = get_rand_target(variables, access_type.pick());

        switch (opType)
	  {
	  case OpType::Left: // var_18 = -var_19;
            {
	      code << target
		   << " = "
		   << op
		   << get_rand_operand(variables, access_type.pick())
		   << ";\n";
	      break;
            }
	  case OpType::FloatLeft: // var_18 = -var_19;
            {
	      code << target
		   << " = "
		   << op
		   <<  get_rand_float_variable(variables)
		   << ";\n";
	      break;
            }
	  case OpType::FloatAssignment: // var_18 = var_19;
            {
	      target = get_rand_float_variable(variables);
	      code << target
		   << " = "
		   <<  get_rand_float_operand(variables)
		   << ";\n";
	      break;
            }
	  case OpType::Binary: // var_18 = var_19 * var_20;
            {
	      if (op=="%") {
		// Some configurations generate calls to emulated software, try to avoid them
		target = get_rand_any_variable(variables);
		code << target
		     << " = "
		     << get_rand_any_variable(variables)
		     << " "
		     << op
		     << " "
		     << std::to_string(rand() % 0xFFFF)
		     << ";\n";
	      } else {
		code << target
		     << " = "
		     << get_rand_operand(variables, access_type.pick())
		     << " "
		     << op
		     << " "
		     << get_rand_operand(variables, access_type.pick())
		     << ";\n";
	      }
	    }
	    break;
	  case OpType::FloatBinary: // var_18 = var_19 * var_20;
            {
	      target = get_rand_float_variable(variables);
	      code << target
		   << " = "
		   << get_rand_float_variable(variables)
		   << " "
		   << op
		   << " "
		   << get_rand_operand(variables, access_type.pick())
		   << ";\n";
	      break;
            }
	  case OpType::ConstantBinary: // var_18 = var_19 + 1;
            {
	      code << target
		   << " = "
		   << get_rand_operand(variables, access_type.pick())
		   << " "
		   << op
		   << " "
		   << get_rand_const_operand(access_type.pick())
		   << ";\n";
	      break;
            }
	  case OpType::Ternary: // var_18 = (var_19 <= var_19) : var_20 : var_21;
            {
	      code << target
		   << " = "
		   << "( "
		   << if_conditions.pick_string() 
		   << " ) ? "
		   << get_rand_operand(variables, AccessType::Normal)
		   << " : "
		   << get_rand_operand(variables, AccessType::Normal)
		   << ";\n";
	      break;
            }
	  case OpType::StructureAssignment: // my_struct1 = mystruct2; just to generate ldms
            {
	      code << "my_struct1 = my_struct2;\n";
	      break;
            }
	  case OpType::InplaceLeft: // ++var_18;
            {
	      
	      code << op
		   << target
		   << ";\n";
	      break;
            }
	  case OpType::InplaceBinary: // var_18 += var_19;
            {
	      
	      code << target
		   << " "
		   << op
		   << " "
		   << get_rand_operand(variables, access_type.pick())
		   << ";\n";
	      break;
            }
	  default:
	    assert(false);
	  }
    }
    if(num_lines_until_end_if > 0)
    {
        code << "}\n";
    }
#if LOG
    std::clog << "Instruction " << num_op << "/" << num_op << std::endl;
#endif
}
