#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream>
#include <sstream>
#include <cassert>

#include "code_gen.h"

#define GREEN "\e[1m\e[32m"
#define DEFAULT "\e[0m\e[39m"

int random(int min, int max) //range : [min, max)
{
    static bool first = true;
    if(first)
    {
        srand(0);
        first = false;
    }
    return min + rand() % ((max + 1) - min);
}

int main(int argc, char* argv[])
{
    assert(argc == 4 && "Usage: c_generator.out generated_path num_files config.json");
    const std::string path(argv[1]);
    const int num_files = atoi(argv[2]);
    const std::string config_file(argv[3]);

    std::clog << "Generated path: " << path << std::endl;
    std::clog << "Number of benchmarks to create: " << num_files << std::endl;
    std::clog << "Config: " << config_file << std::endl;

    struct stat info{};
    if(stat(path.c_str(), &info) != 0)
    {
        printf("cannot access %s\n", path.c_str());
        return 1;
    }
    else if(!(info.st_mode & S_IFDIR))
    {
        printf("%s is no directory\n", path.c_str());
        return 1;
    }

    json config;
    {
        std::ifstream config_stream(config_file);
        config_stream >> config;
    }

    json::reference variables = config["num_variables"];
    const int min_variables = variables["min"].get<int>();
    const int max_variables = variables["max"].get<int>();

    json::reference operations = config["num_operations"];
    const int min_operations = operations["min"].get<int>();
    const int max_operations = operations["max"].get<int>();

    for (int file_index = 7500; file_index < num_files+7500; file_index++)
    {
        std::clog << GREEN << "Creating file #" << file_index << DEFAULT << std::endl;

        int num_variables = random(min_variables, max_variables);
        int num_op = random(min_operations, max_operations);
        std::clog << GREEN << "Creating file #" << num_variables << DEFAULT << std::endl;
        std::clog << GREEN << "Creating file #" << num_op  << DEFAULT << std::endl;

        std::ofstream definitions;
	std::ofstream code;

        std::ostringstream def_filename;
        def_filename << "generated_" << file_index << "_def.h";
        std::ostringstream code_filename;
        code_filename << "generated_" << file_index << "_code.h";

        definitions.open(path + "/" + def_filename.str());
        code.open(path + "/" + code_filename.str());
        {
	  create_file(num_variables, num_op, config, definitions, code);
          std::clog << GREEN << "Creating file done #" << file_index << DEFAULT << std::endl;

        }
        definitions.close();
        code.close();
    }
}
