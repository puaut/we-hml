#pragma once

#include <fstream>
#include "json.h"

using json = nlohmann::json;

void create_file(
        int num_variables,
        int num_op,
        const json& config,
        std::ofstream& definitions,
        std::ofstream& code);
