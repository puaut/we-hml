#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/random.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/random.h>
// From https://android.googlesource.com/device/ti/bootloader/uboot/+/c0eec2d5698a6722a195f4545064dccfb4010c16/arch/arm/cpu/armv7/cache_v7.c

/* Cortex-A9 revisions */
#define MIDR_CORTEX_A9_R0P1	0x410FC091
#define MIDR_CORTEX_A9_R1P2	0x411FC092
#define MIDR_CORTEX_A9_R1P3	0x411FC093
#define MIDR_CORTEX_A9_R2P10	0x412FC09A

/* CCSIDR */
#define CCSIDR_LINE_SIZE_OFFSET		0
#define CCSIDR_LINE_SIZE_MASK		0x7
#define CCSIDR_ASSOCIATIVITY_OFFSET	3
#define CCSIDR_ASSOCIATIVITY_MASK	(0x3FF << 3)
#define CCSIDR_NUM_SETS_OFFSET		13
#define CCSIDR_NUM_SETS_MASK		(0x7FFF << 13)

/*
 * Values for InD field in CSSELR
 * Selects the type of cache
 */
#define ARMV7_CSSELR_IND_DATA_UNIFIED	0
#define ARMV7_CSSELR_IND_INSTRUCTION	1

/* Values for Ctype fields in CLIDR */
#define ARMV7_CLIDR_CTYPE_NO_CACHE		0
#define ARMV7_CLIDR_CTYPE_INSTRUCTION_ONLY	1
#define ARMV7_CLIDR_CTYPE_DATA_ONLY		2
#define ARMV7_CLIDR_CTYPE_INSTRUCTION_DATA	3
#define ARMV7_CLIDR_CTYPE_UNIFIED		4

/*
 * CP15 Barrier instructions
 * Please note that we have separate barrier instructions in ARMv7
 * However, we use the CP15 based instructtions because we use
 * -march=armv5 in U-Boot
 */
#define CP15ISB	asm volatile ("mcr     p15, 0, %0, c7, c5, 4" : : "r" (0))
#define CP15DSB	asm volatile ("mcr     p15, 0, %0, c7, c10, 4" : : "r" (0))
#define CP15DMB	asm volatile ("mcr     p15, 0, %0, c7, c10, 5" : : "r" (0))

#define DEBUG_FLUSH_CACHE 0

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

static inline s32 log_2_n_round_up(u32 n)
{
    s32 log2n = -1;
    u32 temp = n;

    while (temp) {
        log2n++;
        temp >>= 1;
    }

    if (n & (n - 1))
        return log2n + 1; /* not power of 2 - round up */
    else
        return log2n; /* power of 2 */
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

static inline s32 pow_2_n_round_up(u32 n)
{
    u32 temp = n;
    u32 res=1;

    while (temp) {
        temp >>= 1;
	res <<=1;
    }

    if (n & (n - 1))
        return res; /* not power of 2 - round up */
    else
        return res >> 1; /* power of 2 */
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// https://android.googlesource.com/device/ti/bootloader/uboot/+/c0eec2d5698a6722a195f4545064dccfb4010c16/arch/arm/cpu/armv7/cache_v7.c

#define ARMV7_DCACHE_INVAL_ALL		1
#define ARMV7_DCACHE_CLEAN_INVAL_ALL	2
#define ARMV7_DCACHE_INVAL_RANGE     	3
#define ARMV7_DCACHE_CLEAN_INVAL_RANGE	4

/*
 * Write the level and type you want to Cache Size Selection Register(CSSELR)
 * to get size details from Current Cache Size ID Register(CCSIDR)
 */
static void set_csselr(u32 level, u32 type)
{	u32 csselr = level << 1 | type;

    /* Write to Cache Size Selection Register(CSSELR) */
    asm volatile ("mcr p15, 2, %0, c0, c0, 0" : : "r" (csselr));
}

static u32 get_ccsidr(void)
{
    u32 ccsidr;

    /* Read current CP15 Cache Size ID Register */
    asm volatile ("mrc p15, 1, %0, c0, c0, 0" : "=r" (ccsidr));
    return ccsidr;
}

static u32 get_clidr(void)
{
    u32 clidr;

    /* Read current CP15 Cache Level ID Register */
    asm volatile ("mrc p15,1,%0,c0,c0,1" : "=r" (clidr));
    return clidr;
}

static void v7_clean_inval_dcache_level_setway(u32 level, u32 num_sets,
                                               u32 num_ways, u32 way_shift,
                                               u32 log2_line_len)
{
    int way, set, setway;

    /*
     * For optimal assembly code:
     *	a. count down
     *	b. have bigger loop inside
     */
    for (way = num_ways - 1; way >= 0 ; way--) {
        for (set = num_sets - 1; set >= 0; set--) {
            setway = (level << 1) | (set << log2_line_len) |
                     (way << way_shift);
            /*
             * Clean & Invalidate data/unified
             * cache line by set/way
             */
            asm volatile ("	mcr p15, 0, %0, c7, c14, 2"
            : : "r" (setway));
        }
    }
    /* DSB to make sure the operation is complete */
    CP15DSB;
}

static void v7_inval_dcache_level_setway(u32 level, u32 num_sets,
                                         u32 num_ways, u32 way_shift,
                                         u32 log2_line_len)
{
    int way, set, setway;

    /*
     * For optimal assembly code:
     *	a. count down
     *	b. have bigger loop inside
     */
    for (way = num_ways - 1; way >= 0 ; way--) {
        for (set = num_sets - 1; set >= 0; set--)
        {
            setway = (level << 1) | (set << log2_line_len) |
                     (way << way_shift);
            /* Invalidate data/unified cache line by set/way */
            asm volatile ("	mcr p15, 0, %0, c7, c6, 2"
            : : "r" (setway));
        }
    }
    /* DSB to make sure the operation is complete */
    CP15DSB;
}

static void v7_maint_dcache_level_setway(u32 level, u32 operation)
{
    u32 ccsidr;
    u32 num_sets, num_ways, log2_line_len, log2_num_ways;
    u32 way_shift;

    set_csselr(level, ARMV7_CSSELR_IND_DATA_UNIFIED);

    ccsidr = get_ccsidr();

    log2_line_len = ((ccsidr & CCSIDR_LINE_SIZE_MASK) >>
                                                      CCSIDR_LINE_SIZE_OFFSET) + 2;
    /* Converting from words to bytes */
    log2_line_len += 2; // = x4

    num_ways  = ((ccsidr & CCSIDR_ASSOCIATIVITY_MASK) >>
                                                      CCSIDR_ASSOCIATIVITY_OFFSET) + 1;
    num_sets  = ((ccsidr & CCSIDR_NUM_SETS_MASK) >>
                                                 CCSIDR_NUM_SETS_OFFSET) + 1;

    /*
     * According to ARMv7 ARM number of sets and number of ways need
     * not be a power of 2
     */
    log2_num_ways = log_2_n_round_up(num_ways);

    way_shift = (32 - log2_num_ways);
    if (operation == ARMV7_DCACHE_INVAL_ALL) {
        v7_inval_dcache_level_setway(level, num_sets, num_ways,
                                     way_shift, log2_line_len);
    } else if (operation == ARMV7_DCACHE_CLEAN_INVAL_ALL) {
        v7_clean_inval_dcache_level_setway(level, num_sets, num_ways,
                                           way_shift, log2_line_len);
    }
}

static void v7_maint_dcache_all(u32 operation)
{
#if DEBUG_FLUSH_CACHE
    printk(KERN_DEBUG "START_CLEAR");
#endif

    u32 level, cache_type, level_start_bit = 0;

    u32 clidr = get_clidr();

    for (level = 0; level < 6; level++) {
        cache_type = (clidr >> level_start_bit) & 0x7;
        if ((cache_type == ARMV7_CLIDR_CTYPE_DATA_ONLY) ||
            (cache_type == ARMV7_CLIDR_CTYPE_INSTRUCTION_DATA) ||
            (cache_type == ARMV7_CLIDR_CTYPE_UNIFIED))
        {
#if DEBUG_FLUSH_CACHE
            printk(KERN_DEBUG
            "level=%u", level);
#endif
            v7_maint_dcache_level_setway(level, operation);
        }
        level_start_bit += 3;
    }
#if DEBUG_FLUSH_CACHE
    printk(KERN_DEBUG "END_CLEAR");
    printk(KERN_DEBUG "FLUSH");
#endif
}

static void invalidate_dcache_all(void)
{
    v7_maint_dcache_all(ARMV7_DCACHE_CLEAN_INVAL_ALL);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void invalidate_icache_all(void)
{
	/*
	 * Invalidate all instruction caches to PoU.
	 * Also flushes branch target cache.
	 */
	asm volatile ("mcr p15, 0, %0, c7, c5, 0" : : "r" (0));
	/* Invalidate entire branch predictor array */
	asm volatile ("mcr p15, 0, %0, c7, c5, 6" : : "r" (0));
	/* Full system DSB - make sure that the invalidation is complete */
	CP15DSB;
	/* ISB - make sure the instruction stream sees it */
	CP15ISB;
}

///////////////////////////////////////////////////////////////////////////////

#pragma GCC optimize ("O0")

// Management of parameters
// -------------------------
static char* timingml_name = "NONE";
module_param(timingml_name, charp, 0000);
MODULE_PARM_DESC(timingml_name, "timingml_name desc");

long dcache_pollution;
module_param(dcache_pollution, long, 0);
MODULE_PARM_DESC(dcache_pollution, "dcache_pollution desc");

long nb_iteration;
module_param(nb_iteration, long, 0);
MODULE_PARM_DESC(nb_iteration, "nb_iteration desc");

long size_in_bytes;
module_param(size_in_bytes, long, 0);
MODULE_PARM_DESC(size_in_bytes, "size_in_bytes desc");

// Reading of cycle counter
// -------------------------
__attribute__((always_inline)) inline void ccnt_read(volatile unsigned* ptr)
{
    asm volatile ("mrc p15, 0, %0, c9, c13, 0" : "=r" (*ptr));
}

// Random function
// -------------------------
/*
static unsigned int rand(void)
{
    unsigned int i;
    get_random_bytes(&i, sizeof(i));
    return i;
}
*/

#include "GENERATED_DEFINITIONS"

// Cache pollution code
// --------------------
#define BIG_ARRAY_SIZE (32*1024)
static char big_array[BIG_ARRAY_SIZE][64];

uint32_t g_seed=0;
uint32_t r;
inline uint32_t fastrand(void) {
  get_random_bytes(&r, sizeof(r));
  return r;
}

// Flushes the cache hierarchy randomly
// NB: pollution_data is expressed as number of cache lines
// ---------------------------------------
void flush_L1_cache_random(long pollution_data) {
  int i;
  unsigned int idx=0;
  // If pollution data = maximal, scan the entire array
  if (pollution_data >= BIG_ARRAY_SIZE) {
    for (i = 0; i < BIG_ARRAY_SIZE; i++) big_array[i][0] += 77;
  }
  else {
    // Pollute data cache (random technique)
    for (i = 0; i < pollution_data; i++) {
      //idx = (idx+rnd) % BIG_ARRAY_SIZE;
      idx = fastrand()% BIG_ARRAY_SIZE;
      big_array[idx][0] += 77;
    }
  }
  /* DSB to make sure the operation is complete */
  CP15DSB;
}

// --------------------------------------------------------------
//
// Flushes the cache hierarchy randomly, but only target the sets
// touched by the BBs
// - first_address = first address touched by the BB
// - size_in_bytes = number of bytes
// NB1: requires knowledge of structure of L2, cf next macros
// NB2: pollution_data is expressed as number of cache lines
// --------------------------------------------------------------

// Characteristics of cache architecture of L2 on Raspberry Pi 3B+
// ---------------------------------------------------------------
#define BITS_OFFSET 6
#define MASK_OFFSET_6bits 0x3f
#define MASK_IDX_9bits 0x7f
#define LINE_SIZE 64
#define ASSOC 16
#define NB_SETS 512
#define OFFSET_SAME_SET_IN_LINES 0x200

// Useful macros
// -------------
#define set(x) (((x)>>(BITS_OFFSET)) & (MASK_IDX_9bits))
#define datasize_to_lines(s) (((s)%(LINE_SIZE)) ? (s)/(LINE_SIZE)+1: (s)/(LINE_SIZE))

void flush_L1_cache(long pollution_data) {
  // If pollution data = maximal, scan the entire array
  // --------------------------------------------------
  if (pollution_data >= BIG_ARRAY_SIZE) {
    int i;
    for (i = 0; i < BIG_ARRAY_SIZE; i++) big_array[i][0] += 77;
  }
  else {
    // Pollute data cache (random technique, limited to the sets touched by the BB)
    // ----------------------------------------------------------------------------

    // Compute useful values
    // ----------------------
    // Start and end set (for end and start values)
    // NB: we do not necessarily have s_fin > e_deb, depends on mapping
    unsigned long first_address;
    unsigned long s_deb;
    unsigned long s_fin;
    unsigned long s_deb_pollution;
    unsigned long lines_to_skip;
    unsigned long first_conflict_index;
    unsigned int i;
    unsigned long nb_sets;

    extern unsigned long toto;
    first_address = (long) (&toto);
    //printk(KERN_DEBUG "ADDRESS=%lu\n",(unsigned long)first_address);
    //printk(KERN_DEBUG "ADDZERO=%lu\n",(unsigned long)&zero);
    //printk(KERN_DEBUG "SIZE=%ld\n",size_in_bytes);

    s_deb = set(first_address);
    s_fin = set(first_address+size_in_bytes-1);
    
    // Number of sets touched by basic block
    if (s_fin >= s_deb) nb_sets = s_fin - s_deb + 1;
    else nb_sets = s_fin + 512 - s_deb + 1;
    
    // Get set for first line of pollution array
    s_deb_pollution = set((unsigned long)(big_array));
    
    // Calculate index in pollution array to reach first set of data
    if (s_deb_pollution <= s_deb) lines_to_skip = s_deb - s_deb_pollution;
    else lines_to_skip = s_deb + NB_SETS - s_deb_pollution;
    
    // First set in conflict with data
    first_conflict_index = lines_to_skip;
    
    // Pollution loop
    for (i = 0;i<pollution_data;i++) {
      unsigned long polluted_set_index;
      unsigned long frame;
      unsigned long idx_in_pollution_array;
      
      polluted_set_index = fastrand() % datasize_to_lines(size_in_bytes);

      // Select one of the possible frame in pollution array
      frame = fastrand() % (BIG_ARRAY_SIZE / LINE_SIZE);
 
      // Calculate the index in pollution array and actually pollute
      idx_in_pollution_array = (first_conflict_index + polluted_set_index + frame*OFFSET_SAME_SET_IN_LINES)
	% BIG_ARRAY_SIZE;
      big_array[idx_in_pollution_array][0] += 77;
      
    }
  }
  /* DSB to make sure the operation is complete */
  CP15DSB;
}

__attribute__((always_inline)) inline void irq_enable(void)
{
	__asm__ __volatile__ ("cpsie i");
}

__attribute__((always_inline)) inline void irq_disable(void)
{
	__asm__ __volatile__ ("cpsid i");
}

// The code itself
// ----------------

static void the_code(void) {
  asm("ldrsh	r3, [sp,#0]");
#include "GENERATED_CODE"
  asm("rsblt r3,r3,#0");
  asm("nop");
  asm("ubfx r0,r0,#0,#13");
}

static noinline void function(int usage)
{
  volatile unsigned before, after;
  int i;
  if (nb_iteration==1) {
    //irq_disable();

    ccnt_read(&before);
    the_code();
    ccnt_read(&after);

    //irq_enable();
    printk(KERN_DEBUG "NAME=%s; INDEX=%d; RESULT=%u\n", timingml_name, 0, after-before);
  }
  else {
    for (i = 0; i < nb_iteration+1; i++) {
      //irq_disable();

      ccnt_read(&before);
      the_code();
      ccnt_read(&after);

      //irq_enable();
      if (i!=0) {
	printk(KERN_DEBUG "NAME=%s; INDEX=%d; RESULT=%u\n", timingml_name, i-1, after-before);
      }
      invalidate_icache_all();
      flush_L1_cache_random(dcache_pollution);
    }
  }
}

static void timing_patch(void* data)
{
  int i;
  // Does not change much, removed pollution correction
  // --------------------------------------------------
  //printk(KERN_DEBUG "POLLUTION initiale %ld\n",dcache_pollution);
  //dcache_pollution = pow_2_n_round_up(dcache_pollution);
  //printk(KERN_DEBUG "POLLUTION corrigee %ld\n",dcache_pollution);
  // Execute code on core 0 only
  if (smp_processor_id() != 3) return;
  flush_L1_cache(BIG_ARRAY_SIZE);
  invalidate_dcache_all();
  invalidate_icache_all();
  printk(KERN_DEBUG "TIMINGML_START\n");
  function(dcache_pollution);
  printk(KERN_DEBUG "TIMINGML_END\n");
}

int init_module()
{
    on_each_cpu(timing_patch, NULL, 1);
    return 0;
}

void cleanup_module()
{
}

MODULE_LICENSE("GPL");
