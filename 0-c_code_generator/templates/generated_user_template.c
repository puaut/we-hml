#include <stdio.h>
#include <stdint.h>
#include <string.h>

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#pragma GCC optimize ("O0")

#include "GENERATED_DEFINITIONS"

// To pollute the cache
#define BIG_ARRAY_SIZE 16384
static char big_array[BIG_ARRAY_SIZE][64];

__attribute__((always_inline)) inline void ccnt_read(volatile unsigned* ptr)
{
    asm volatile ("mrc p15, 0, %0, c9, c13, 0" : "=r" (*ptr));
}

long dcache_pollution = 0;

// NB: pollution_data is expressed as number of cache lines
void flush_L1_cache(long pollution_code, long pollution_data) {
  int i;
  // Pollute data cache (random technique)
  for (i = 0; i < pollution_data && i < BIG_ARRAY_SIZE; i++) {
    int idx = random() % BIG_ARRAY_SIZE;
    big_array[idx][0] += 77;
  }
  // Pollute instruction cache
  // Right now, execute a sufficiently long sequence of instructions
#define NOP __asm__("nop")
#define sixteen(a) a;a;a;a;a;a;a;a;a;a;a;a
#define four_k(a) sixteen(sixteen(sixteen(a)))
four_k(NOP);
four_k(NOP);
}

static void the_code(void) {
  asm("ldrsh	r3, [sp,#0]");
#include "GENERATED_CODE"
  asm("rsblt r3,r3,#0");
}

static uint64_t function(char *basename)
{
  volatile unsigned before, after;
  int i;
  printf("[37.15] TIMINGML_START\n");
  for(i = 0; i < LOOP_ITERATIONS+1; i++) {
    ccnt_read(&before);
    the_code();    
    ccnt_read(&after);
    if (i!=0) {
      printf("[37.15] NAME = %s; INDEX=%d; RESULT=%u\n",basename,i-1,after - before);
    }
    flush_L1_cache(0,dcache_pollution);
  }
  printf("[37.15] TIMINGML_END\n");
}

int main(int argc, void**argv) {
  if (argc!=3) {
    printf("Usage %s benchname dcachepollution\n");
  }
  else {
    dcache_pollution = atol(argv[2]) / 64;
    uint64_t result = function(argv[1]);
  }
}
