
FIXME: templates for benchmarks and sampling data should be the same
(except parameter specifying cache pollution, benchmarks should not have the 'nb_iterations' parameter)

bench_kernel_template.c		Code template to generate benchmarks in user and kernel mode
bench_user_template.c

generated_kernel_template.c	Code template to generate sampling data in user and kernel mode
generated_user_template.c

kernel_module_makefile		Template for compiling kernel modules
