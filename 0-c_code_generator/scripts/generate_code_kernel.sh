#!/usr/bin/env bash

# ------------------------------------------------------------------
# Generate sampling data (basic blocks) to be run in kernel mode
# ------------------------------------------------------------------

set -e

# Removed these colors (Linux specific)
MAGENTA='\e[1m\e[95m'
BOLD_WHITE='\e[1m'
DEFAULT='\e[0m\e[39m'

if [[ "$#" -ne 5 ]]; then
    echo "Usage: generate_code_kernel.sh GENERATED_PATH CONFIG_FILE LOOP_ITERATIONS NUMBER_BENCHMARKS CROSS_COMPILE"
    exit
fi

GENERATED_PATH=$1
CONFIG_FILE=$2
LOOP_ITERATIONS=$3
NUMBER_BENCHMARKS=$4
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CROSS_COMPILE=$5

echo "Generated path: $GENERATED_PATH"
echo "Config file: $CONFIG_FILE"
echo "Number of loop iterations: $LOOP_ITERATIONS"
echo "Number of benchmarks: $NUMBER_BENCHMARKS"
echo "Cross-compiler: $CROSS_COMPILE"

cd ${DIR}

if [[ -d ${GENERATED_PATH} ]]; then
    echo -e "${BOLD_WHITE}Remove $GENERATED_PATH${DEFAULT}? "
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) rm -rf ${GENERATED_PATH}; break;;
            No ) exit;;
        esac
    done
fi

mkdir ${GENERATED_PATH}

cp ${CONFIG_FILE} ${GENERATED_PATH}

echo "$0 $@" > ${GENERATED_PATH}/cmdline.sh

echo -e "${MAGENTA}Compiling code generator...${DEFAULT}"

CMAKE_BUILD_DIR=${DIR}/../c_generator/
cd ${CMAKE_BUILD_DIR}
make -f ${CMAKE_BUILD_DIR}/Makefile

echo -e "${MAGENTA}Generating files...${DEFAULT}"
${CMAKE_BUILD_DIR}/c_generator ${GENERATED_PATH} ${NUMBER_BENCHMARKS} ${CONFIG_FILE}

export ARCH=arm
export KERNEL=kernel7

echo -e "${MAGENTA}Compiling files...${DEFAULT}"
${DIR}/../c_generator/compile_kernel.sh ${DIR}/../templates/generated_kernel_template.c ${GENERATED_PATH} ${LOOP_ITERATIONS} ${CROSS_COMPILE}

echo -e "${MAGENTA}Generating & parsing objdumps...${DEFAULT}"
${DIR}/../parser/parser.sh ${GENERATED_PATH} ${CROSS_COMPILE} "function" KERNEL
echo -e "${MAGENTA}Done${DEFAULT}"

