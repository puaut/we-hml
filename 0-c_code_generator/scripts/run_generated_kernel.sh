#!/usr/bin/env bash

# ----------------------------------------------------------
# Run sampling data (basic blocks) when generated in kernel mode
# ----------------------------------------------------------

sudo dmesg -c

for file in *.ko; do
    echo $file
    sudo insmod $file timingml_name=${file} dcache_pollution=0 nb_iteration=1000
    sudo rmmod $file
    sudo dmesg -c > ${file}_res.txt
done
