

bench_cross_compile_user.sh	compile benchmarks in user mode

bench_plot.py			Plot the frequency of instructions in a set of json files

filter.py			Filter outliers in result files (.txt) and write back in same file
filter.sh

generate_code_kernel.sh		Generate sampling data (basic blocks) in user and kernel mode
generate_code_user.sh


run_benchmarks_kernel.sh	Execute benchmarks (in kernel and user mode)
run_benchmarks_user.sh

run_generated_kernel.sh		Execute sampling data in kernel mode, several iterations
run_generated_nocache.sh	Execute sampling data in kernel mode, no iterations (empty cache)
run_pollution.py		Execute sampling data in kernel mode, different pollution degrees
run_pollution.sh

plot.sh				Plot result files
plot_nb_tests.py

plot_cache.py			Plot variation of worst-case timing in function of cache pollution
