import pickle 
import sys 
import math 
import os
import pandas as pd
from utils.tools import make_x

#assert False, "You need to hardcode the classifiers path in find_wcet.py"
source_folder = "/home/paranwin/pi/precomputed/"
#source_folder = "path_to_classifiers"

classifier_name = "classifier_Random Forest"
#classifier_name = "classifier_Neural Network"
#classifier_name = "classifier_Gradient Boosting regressor"
#classifier_name = "classifier_Bayesian Ridge"
#classifier_name = "classifier_Ridge"


assert len(sys.argv) == 4, "Usage: find_wcet.py instructions.txt previous_instructions.txt result.txt"

instructions_file = sys.argv[1]
prev_instructions_file = sys.argv[2]
result = sys.argv[3]

with open(source_folder + "global_instructions.pickle", "rb") as file:
    global_instructions = pickle.load(file)

with open(source_folder + "global_instructions_indices.pickle", "rb") as file:
    global_instructions_indices = pickle.load(file)

#print("Instructions:")
instructions_list = []
with open(instructions_file, "r") as f:
    for line in f.readlines():
        #if line[-1] == "\n":
        #    line = line[:-1]
        #if len(instructions_list) < 25:
        #    print("     ", line)
        #elif len(instructions_list) == 25:
        #    print("      ...")
        instructions_list.append(line.split(" ")[0])

#print("Previous instructions:")
previous_instructions = []
with open(prev_instructions_file, "r") as f:
    for line in f.readlines():
        #if line[-1] == "\n":
        #    line = line[:-1]
        #if len(previous_instructions) < 25:
        #    print("     ", line)
        #elif len(previous_instructions) == 25:
        #    print("      ...")
        previous_instructions.append(line.split(" ")[0])

cache_used_BB = instructions_list.count("str") + instructions_list.count("ldr")
cache_used = previous_instructions.count("str") + previous_instructions.count("ldr")
if cache_used_BB !=0:
    cache_pollution = math.ceil(cache_used / cache_used_BB)
else:
    cache_pollution = 0
# NB: cache_pollution has value 0 if BB not in loop or does not use data
#print("Cache_used_BB: ", cache_used_BB)
#print("Cache_used: ", cache_used)
#print("Cache pollution: ", cache_pollution)

instructions_dict = {instruction: 0 for instruction in instructions_list}
for instruction in instructions_dict.keys():
    instructions_dict[instruction] = instructions_list.count(instruction)

#HACK: ignore these instructions
for instr in ["bx"]:
    if instr in instructions_dict:
        del instructions_dict[instr]
    
for instr in list(instructions_dict):
    if instr not in global_instructions:
        print("here")
       # input("Deleting " + instr + "\n")
        del instructions_dict[instr]

# Use classifier correct classifier file
# - cold cache (pollution 0, by convention)
# - pollution: power up to the >= power of two (if not in loop,
#   ie cache_pollution is 0, use cold cache value)
if cache_pollution == 0 :
    classifier_file_cold = classifier_name+"_512"+".pickle"
else :
    classifier_file_cold = classifier_name+"_512"+".pickle"

with open(source_folder + classifier_file_cold, "rb") as file:
    classifier_cold = pickle.load(file)

first_x, first_size = make_x(global_instructions,
                             global_instructions_indices,
                             instructions_dict)
for i in range(len(first_x)):
    if first_x[i] == 0 and first_size != 0:
        first_x[i] = 0.0001 / first_size

wcet_first = classifier_cold.predict([first_x])[0] * first_size
#wcet_first=451

pollution = 1
print("coucou")
print(cache_pollution)
while cache_pollution > pollution:
    pollution = pollution *2
if cache_pollution==0:
    pollution=1
if cache_pollution > 512:
    pollution = 512
classifier_file = classifier_name+"_"+str(512)+".pickle"
if not os.path.exists(source_folder + classifier_file):
    print("Classifier file: ", classifier_file)
    print("Classifier for pollution does not exist, considering cold cache:",pollution)
    classifier_file = classifier_file_cold
with open(source_folder + classifier_file, "rb") as file:
    classifier = pickle.load(file)
second_x, second_size = make_x(global_instructions,
                               global_instructions_indices,
                               instructions_dict)
for i in range(len(second_x)):
    if second_x[i] == 0 and second_size != 0:
        second_x[i] = 0.0001 / second_size
wcet_next = classifier.predict([second_x])[0] * second_size
#wcet_next = 451
#print(classifier.predict([second_x]))
#print(second_size)

if (wcet_first < wcet_next):
    wcet_first = wcet_next
#wcet_first = min(wcet_first,wcet_next)
#wcet_next = wcet_first
#print("WCET first: ", wcet_first)
#print("WCET next: ", wcet_next)

with open(result, "w") as f:
    f.write(str(wcet_first) + "\n" + str(wcet_next))
