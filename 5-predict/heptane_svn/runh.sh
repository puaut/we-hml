#!/usr/bin/env bash

export TIMING_ML=/home/paranwin/pi/scripts/find_wcet.py

# -------------------------------
#
# A regarder (ordre alphabetique)
#
# -------------------------------

# Utilise des instructions émulées (aeabi_uidiv)
# ---------------
#source run.sh ../benchmarks/ammunition/ ammunition.c.orig
#source run.sh ../benchmarks/anagram/ anagram.c.orig
#source run.sh ../benchmarks/cjpeg_transupp/ cjpeg_transupp.c.orig

# Recursive
# ---------------
#source run.sh ../benchmarks/bitonic bitonic.c.orig
#source run.sh ../benchmarks/fac/ fac.c.orig
#source run.sh ../benchmarks/recursion/ recursion.c.orig

# Uses memcpy
# -----------
#source run.sh ../benchmarks/cjpeg_wrbmp/ cjpeg_wrbmp.c.orig

# Fait planter le solveur
# -----------------------
#source run.sh ../benchmarks/rijndael_dec/ rijndael_dec.c.orig
#source run.sh ../benchmarks/rijndael_enc/ rijndael_enc.c.orig

# Double call in loop
# -------------------
#source run.sh ../benchmarks/adpcm/ adpcm.c.orig
#source run.sh ../benchmarks/cnt/ cnt.c.orig
#source run.sh ../benchmarks/compress/ compress.c.orig
#source run.sh ../benchmarks/dijkstra/ dijkstra.c.orig
#source run.sh ../benchmarks/huff_dec/ huff_dec.c.orig
#source run.sh ../benchmarks/adpcm_dec/ adpcm_dec.c.orig
#source run.sh ../benchmarks/g723_enc/ g723_enc.c.orig
#source run.sh ../benchmarks/prime/ prime.c.orig
#source run.sh ../benchmarks/sha/ sha.c.orig
#source run.sh ../benchmarks/md5/ md5.c.orig
#source run.sh ../benchmarks/mpeg2/ mpeg2.c.orig


# OK: ordre alphabetique
# -----------------------
source run.sh ../../Papier/test_hybride/ testh.c.orig
#source run.sh benchmarks/binarysearch binarysearch.c.orig
#source run.sh benchmarks/bs/ bs.c.orig
#source run.sh benchmarks/bsort/ bsort.c.orig
#source run.sh benchmarks/bsort100/ bsort100.c.orig
#source run.sh benchmarks/countnegative/ countnegative.c.orig
#source run.sh benchmarks/crc/ crc.c
#source run.sh benchmarks/expint/ expint.c.orig
#source run.sh benchmarks/expint_nip/ expint_nip.c.orig
#source run.sh benchmarks/ fdct/fdct.c.orig
#source run.sh benchmarks/fibcall/ fibcall.c.orig
#source run.sh benchmarks/fir/ fir.c.orig
#source run.sh benchmarks/h264_dec/ h264_dec.c.orig
#source run.sh benchmarks/insertsort/ insertsort.c.orig
#source run.sh benchmarks/jfdctint/ jfdctint.c.orig
#source run.sh benchmarks/matmult/ matmult.c.orig
#source run.sh benchmarks/matrix1/ matrix1.c.orig
#source run.sh benchmarks/minmax/ minmax.c.orig
#source run.sh benchmarks/ns/ ns.c.orig
#source run.sh benchmarks/ns_nip/ ns_nip.c.orig
#source run.sh benchmarks/nsichneu/ nsichneu.c.orig
#source run.sh benchmarks/petrinet/ petrinet.c.orig
