	.cpu arm7tdmi
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.arm
	.syntax divided
	.file	"X_BENCH.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.align	2
	.global	__aeabi_idiv
	.type	__aeabi_idiv, %function
__aeabi_idiv:
.LFB0:
	.file 1 "./ARMlib.h"
	.loc 1 29 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 29 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE0:
	.size	__aeabi_idiv, .-__aeabi_idiv
	.align	2
	.global	__aeabi_i2d
	.type	__aeabi_i2d, %function
__aeabi_i2d:
.LFB1:
	.loc 1 30 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 30 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE1:
	.size	__aeabi_i2d, .-__aeabi_i2d
	.align	2
	.global	__aeabi_uidivmod
	.type	__aeabi_uidivmod, %function
__aeabi_uidivmod:
.LFB2:
	.loc 1 31 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 31 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE2:
	.size	__aeabi_uidivmod, .-__aeabi_uidivmod
	.align	2
	.global	__aeabi_fcmpge
	.type	__aeabi_fcmpge, %function
__aeabi_fcmpge:
.LFB3:
	.loc 1 32 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 32 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE3:
	.size	__aeabi_fcmpge, .-__aeabi_fcmpge
	.align	2
	.global	__aeabi_i2f
	.type	__aeabi_i2f, %function
__aeabi_i2f:
.LFB4:
	.loc 1 33 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 33 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE4:
	.size	__aeabi_i2f, .-__aeabi_i2f
	.align	2
	.global	__aeabi_ddiv
	.type	__aeabi_ddiv, %function
__aeabi_ddiv:
.LFB5:
	.loc 1 37 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 37 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE5:
	.size	__aeabi_ddiv, .-__aeabi_ddiv
	.align	2
	.global	__aeabi_dsub
	.type	__aeabi_dsub, %function
__aeabi_dsub:
.LFB6:
	.loc 1 38 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 38 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE6:
	.size	__aeabi_dsub, .-__aeabi_dsub
	.align	2
	.global	__aeabi_dmul
	.type	__aeabi_dmul, %function
__aeabi_dmul:
.LFB7:
	.loc 1 39 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 39 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE7:
	.size	__aeabi_dmul, .-__aeabi_dmul
	.align	2
	.global	__aeabi_dcmpge
	.type	__aeabi_dcmpge, %function
__aeabi_dcmpge:
.LFB8:
	.loc 1 40 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 40 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE8:
	.size	__aeabi_dcmpge, .-__aeabi_dcmpge
	.align	2
	.global	__aeabi_dcmpeq
	.type	__aeabi_dcmpeq, %function
__aeabi_dcmpeq:
.LFB9:
	.loc 1 41 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 41 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE9:
	.size	__aeabi_dcmpeq, .-__aeabi_dcmpeq
	.align	2
	.global	__aeabi_dcmpgt
	.type	__aeabi_dcmpgt, %function
__aeabi_dcmpgt:
.LFB10:
	.loc 1 42 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 42 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE10:
	.size	__aeabi_dcmpgt, .-__aeabi_dcmpgt
	.align	2
	.global	__aeabi_dcmple
	.type	__aeabi_dcmple, %function
__aeabi_dcmple:
.LFB11:
	.loc 1 43 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 43 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE11:
	.size	__aeabi_dcmple, .-__aeabi_dcmple
	.align	2
	.global	__aeabi_d2f
	.type	__aeabi_d2f, %function
__aeabi_d2f:
.LFB12:
	.loc 1 44 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 44 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE12:
	.size	__aeabi_d2f, .-__aeabi_d2f
	.align	2
	.global	__aeabi_dadd
	.type	__aeabi_dadd, %function
__aeabi_dadd:
.LFB13:
	.loc 1 45 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 45 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE13:
	.size	__aeabi_dadd, .-__aeabi_dadd
	.align	2
	.global	__aeabi_d2iz
	.type	__aeabi_d2iz, %function
__aeabi_d2iz:
.LFB14:
	.loc 1 46 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 46 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE14:
	.size	__aeabi_d2iz, .-__aeabi_d2iz
	.align	2
	.global	__aeabi_fcmplt
	.type	__aeabi_fcmplt, %function
__aeabi_fcmplt:
.LFB15:
	.loc 1 49 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 49 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE15:
	.size	__aeabi_fcmplt, .-__aeabi_fcmplt
	.align	2
	.global	__aeabi_fcmpgt
	.type	__aeabi_fcmpgt, %function
__aeabi_fcmpgt:
.LFB16:
	.loc 1 50 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 50 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE16:
	.size	__aeabi_fcmpgt, .-__aeabi_fcmpgt
	.align	2
	.global	__aeabi_fdiv
	.type	__aeabi_fdiv, %function
__aeabi_fdiv:
.LFB17:
	.loc 1 51 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 51 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE17:
	.size	__aeabi_fdiv, .-__aeabi_fdiv
	.align	2
	.global	__aeabi_fcmpeq
	.type	__aeabi_fcmpeq, %function
__aeabi_fcmpeq:
.LFB18:
	.loc 1 52 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 52 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE18:
	.size	__aeabi_fcmpeq, .-__aeabi_fcmpeq
	.align	2
	.global	__aeabi_fmul
	.type	__aeabi_fmul, %function
__aeabi_fmul:
.LFB19:
	.loc 1 53 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 53 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE19:
	.size	__aeabi_fmul, .-__aeabi_fmul
	.align	2
	.global	__aeabi_fsub
	.type	__aeabi_fsub, %function
__aeabi_fsub:
.LFB20:
	.loc 1 54 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 54 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE20:
	.size	__aeabi_fsub, .-__aeabi_fsub
	.align	2
	.global	__aeabi_f2d
	.type	__aeabi_f2d, %function
__aeabi_f2d:
.LFB21:
	.loc 1 55 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 55 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE21:
	.size	__aeabi_f2d, .-__aeabi_f2d
	.align	2
	.global	__aeabi_fadd
	.type	__aeabi_fadd, %function
__aeabi_fadd:
.LFB22:
	.loc 1 56 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 56 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE22:
	.size	__aeabi_fadd, .-__aeabi_fadd
	.align	2
	.global	__aeabi_ui2
	.type	__aeabi_ui2, %function
__aeabi_ui2:
.LFB23:
	.loc 1 59 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 59 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE23:
	.size	__aeabi_ui2, .-__aeabi_ui2
	.align	2
	.global	__aeabi_ui2d
	.type	__aeabi_ui2d, %function
__aeabi_ui2d:
.LFB24:
	.loc 1 60 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 60 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE24:
	.size	__aeabi_ui2d, .-__aeabi_ui2d
	.align	2
	.global	__aeabi_dcmpun
	.type	__aeabi_dcmpun, %function
__aeabi_dcmpun:
.LFB25:
	.loc 1 62 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 62 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE25:
	.size	__aeabi_dcmpun, .-__aeabi_dcmpun
	.align	2
	.global	__aeabi_dcmplt
	.type	__aeabi_dcmplt, %function
__aeabi_dcmplt:
.LFB26:
	.loc 1 63 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 63 0
	mov	r0, r0	@ nop
	bx	lr
	.cfi_endproc
.LFE26:
	.size	__aeabi_dcmplt, .-__aeabi_dcmplt
	.comm	petrinet_P1_is_marked,4,4
	.comm	petrinet_P1_marking_member_0,12,4
	.comm	petrinet_P2_is_marked,4,4
	.comm	petrinet_P2_marking_member_0,20,4
	.comm	petrinet_P3_is_marked,4,4
	.comm	petrinet_P3_marking_member_0,24,4
	.global	petrinet_CHECKSUM
	.section	.rodata
	.align	2
	.type	petrinet_CHECKSUM, %object
	.size	petrinet_CHECKSUM, 4
petrinet_CHECKSUM:
	.space	4
	.text
	.align	2
	.global	petrinet_main
	.type	petrinet_main, %function
petrinet_main:
.LFB27:
	.file 2 "X_BENCH.c"
	.loc 2 59 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 320
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #320
	.cfi_def_cfa_offset 320
	.loc 2 62 0
	mov	r3, #2
	str	r3, [sp, #316]
	.loc 2 68 0
	b	.L29
.L55:
	.loc 2 69 0
	.syntax divided
@ 69 "X_BENCH.c" 1
	1:
        .section .wcet_annot
        .long 1b
        .long 1
        .long 2
                        .text
@ 0 "" 2
	.loc 2 75 0
	.arm
	.syntax divided
	ldr	r3, [sp, #316]
	sub	r3, r3, #1
	str	r3, [sp, #316]
	.loc 2 78 0
	ldr	r3, .L56
	ldr	r3, [r3]
	cmp	r3, #2
	ble	.L30
	.loc 2 79 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 78 0 discriminator 1
	cmp	r3, #6
	bgt	.L30
	.loc 2 80 0
	ldr	r3, .L56+8
	ldr	r2, [r3, #4]
	.loc 2 81 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #8]
	.loc 2 79 0
	cmp	r2, r3
	bne	.L30
.LBB2:
	.loc 2 87 0
	ldr	r3, .L56+8
	ldr	r3, [r3]
	str	r3, [sp, #312]
	.loc 2 88 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #4]
	str	r3, [sp, #308]
	.loc 2 91 0
	ldr	r2, [sp, #312]
	ldr	r3, [sp, #308]
	cmp	r2, r3
	bge	.L30
	.loc 2 94 0
	ldr	r3, .L56
	ldr	r3, [r3]
	sub	r3, r3, #3
	ldr	r2, .L56
	str	r3, [r2]
	.loc 2 97 0
	ldr	r2, [sp, #312]
	ldr	r3, [sp, #308]
	rsb	r3, r3, r2
	str	r3, [sp, #304]
	.loc 2 100 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #312]
	str	r2, [r1, r3, asl #2]
	.loc 2 101 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #308]
	str	r2, [r1, r3, asl #2]
	.loc 2 102 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #304]
	str	r2, [r1, r3, asl #2]
	.loc 2 103 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L30:
.LBE2:
	.loc 2 110 0
	ldr	r3, .L56
	ldr	r3, [r3]
	cmp	r3, #2
	ble	.L31
	.loc 2 111 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 110 0 discriminator 1
	cmp	r3, #6
	bgt	.L31
	.loc 2 112 0
	ldr	r3, .L56+8
	ldr	r2, [r3, #8]
	.loc 2 113 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #4]
	.loc 2 111 0
	cmp	r2, r3
	bne	.L31
.LBB3:
	.loc 2 119 0
	ldr	r3, .L56+8
	ldr	r3, [r3]
	str	r3, [sp, #300]
	.loc 2 120 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #8]
	str	r3, [sp, #296]
	.loc 2 123 0
	ldr	r2, [sp, #300]
	ldr	r3, [sp, #296]
	cmp	r2, r3
	bge	.L31
	.loc 2 126 0
	ldr	r3, .L56
	ldr	r3, [r3]
	sub	r3, r3, #3
	ldr	r2, .L56
	str	r3, [r2]
	.loc 2 129 0
	ldr	r2, [sp, #300]
	ldr	r3, [sp, #296]
	rsb	r3, r3, r2
	str	r3, [sp, #292]
	.loc 2 132 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #300]
	str	r2, [r1, r3, asl #2]
	.loc 2 133 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #296]
	str	r2, [r1, r3, asl #2]
	.loc 2 134 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #292]
	str	r2, [r1, r3, asl #2]
	.loc 2 135 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L31:
.LBE3:
	.loc 2 142 0
	ldr	r3, .L56
	ldr	r3, [r3]
	cmp	r3, #2
	ble	.L32
	.loc 2 143 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 142 0 discriminator 1
	cmp	r3, #6
	bgt	.L32
	.loc 2 144 0
	ldr	r3, .L56+8
	ldr	r2, [r3]
	.loc 2 145 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #8]
	.loc 2 143 0
	cmp	r2, r3
	bne	.L32
.LBB4:
	.loc 2 151 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #4]
	str	r3, [sp, #288]
	.loc 2 152 0
	ldr	r3, .L56+8
	ldr	r3, [r3]
	str	r3, [sp, #284]
	.loc 2 155 0
	ldr	r2, [sp, #288]
	ldr	r3, [sp, #284]
	cmp	r2, r3
	bge	.L32
	.loc 2 158 0
	ldr	r3, .L56
	ldr	r3, [r3]
	sub	r3, r3, #3
	ldr	r2, .L56
	str	r3, [r2]
	.loc 2 161 0
	ldr	r2, [sp, #288]
	ldr	r3, [sp, #284]
	rsb	r3, r3, r2
	str	r3, [sp, #280]
	.loc 2 164 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #288]
	str	r2, [r1, r3, asl #2]
	.loc 2 165 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #284]
	str	r2, [r1, r3, asl #2]
	.loc 2 166 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #280]
	str	r2, [r1, r3, asl #2]
	.loc 2 167 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L32:
.LBE4:
	.loc 2 174 0
	ldr	r3, .L56
	ldr	r3, [r3]
	cmp	r3, #2
	ble	.L33
	.loc 2 175 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 174 0 discriminator 1
	cmp	r3, #6
	bgt	.L33
	.loc 2 176 0
	ldr	r3, .L56+8
	ldr	r2, [r3, #8]
	.loc 2 177 0
	ldr	r3, .L56+8
	ldr	r3, [r3]
	.loc 2 175 0
	cmp	r2, r3
	bne	.L33
.LBB5:
	.loc 2 183 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #4]
	str	r3, [sp, #276]
	.loc 2 184 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #8]
	str	r3, [sp, #272]
	.loc 2 187 0
	ldr	r2, [sp, #276]
	ldr	r3, [sp, #272]
	cmp	r2, r3
	bge	.L33
	.loc 2 190 0
	ldr	r3, .L56
	ldr	r3, [r3]
	sub	r3, r3, #3
	ldr	r2, .L56
	str	r3, [r2]
	.loc 2 193 0
	ldr	r2, [sp, #276]
	ldr	r3, [sp, #272]
	rsb	r3, r3, r2
	str	r3, [sp, #268]
	.loc 2 196 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #276]
	str	r2, [r1, r3, asl #2]
	.loc 2 197 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #272]
	str	r2, [r1, r3, asl #2]
	.loc 2 198 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #268]
	str	r2, [r1, r3, asl #2]
	.loc 2 199 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L33:
.LBE5:
	.loc 2 206 0
	ldr	r3, .L56
	ldr	r3, [r3]
	cmp	r3, #2
	ble	.L34
	.loc 2 207 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 206 0 discriminator 1
	cmp	r3, #6
	bgt	.L34
	.loc 2 208 0
	ldr	r3, .L56+8
	ldr	r2, [r3]
	.loc 2 209 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #4]
	.loc 2 207 0
	cmp	r2, r3
	bne	.L34
.LBB6:
	.loc 2 214 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #8]
	str	r3, [sp, #264]
	.loc 2 215 0
	ldr	r3, .L56+8
	ldr	r3, [r3]
	str	r3, [sp, #260]
	.loc 2 218 0
	ldr	r2, [sp, #264]
	ldr	r3, [sp, #260]
	cmp	r2, r3
	bge	.L34
	.loc 2 221 0
	ldr	r3, .L56
	ldr	r3, [r3]
	sub	r3, r3, #3
	ldr	r2, .L56
	str	r3, [r2]
	.loc 2 224 0
	ldr	r2, [sp, #264]
	ldr	r3, [sp, #260]
	rsb	r3, r3, r2
	str	r3, [sp, #256]
	.loc 2 227 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #264]
	str	r2, [r1, r3, asl #2]
	.loc 2 228 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #260]
	str	r2, [r1, r3, asl #2]
	.loc 2 229 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #256]
	str	r2, [r1, r3, asl #2]
	.loc 2 230 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L34:
.LBE6:
	.loc 2 237 0
	ldr	r3, .L56
	ldr	r3, [r3]
	cmp	r3, #2
	ble	.L35
	.loc 2 238 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 237 0 discriminator 1
	cmp	r3, #6
	bgt	.L35
	.loc 2 239 0
	ldr	r3, .L56+8
	ldr	r2, [r3, #4]
	.loc 2 240 0
	ldr	r3, .L56+8
	ldr	r3, [r3]
	.loc 2 238 0
	cmp	r2, r3
	bne	.L35
.LBB7:
	.loc 2 245 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #8]
	str	r3, [sp, #252]
	.loc 2 246 0
	ldr	r3, .L56+8
	ldr	r3, [r3, #4]
	str	r3, [sp, #248]
	.loc 2 249 0
	ldr	r2, [sp, #252]
	ldr	r3, [sp, #248]
	cmp	r2, r3
	bge	.L35
	.loc 2 252 0
	ldr	r3, .L56
	ldr	r3, [r3]
	sub	r3, r3, #3
	ldr	r2, .L56
	str	r3, [r2]
	.loc 2 255 0
	ldr	r2, [sp, #252]
	ldr	r3, [sp, #248]
	rsb	r3, r3, r2
	str	r3, [sp, #244]
	.loc 2 258 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #252]
	str	r2, [r1, r3, asl #2]
	.loc 2 259 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #248]
	str	r2, [r1, r3, asl #2]
	.loc 2 260 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #244]
	str	r2, [r1, r3, asl #2]
	.loc 2 261 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L35:
.LBE7:
	.loc 2 268 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L36
	.loc 2 269 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 268 0 discriminator 1
	cmp	r3, #6
	bgt	.L36
	.loc 2 270 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #4]
	.loc 2 271 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	.loc 2 269 0
	cmp	r2, r3
	bne	.L36
	.loc 2 272 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #4]
	.loc 2 273 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 271 0
	cmp	r2, r3
	bne	.L36
.LBB8:
	.loc 2 278 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #240]
	.loc 2 279 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	str	r3, [sp, #236]
	.loc 2 282 0
	ldr	r2, [sp, #236]
	ldr	r3, [sp, #240]
	cmp	r2, r3
	ble	.L36
	.loc 2 285 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 288 0
	ldr	r2, [sp, #240]
	ldr	r3, [sp, #236]
	add	r3, r2, r3
	str	r3, [sp, #232]
	.loc 2 291 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #240]
	str	r2, [r1, r3, asl #2]
	.loc 2 292 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #236]
	str	r2, [r1, r3, asl #2]
	.loc 2 293 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #232]
	str	r2, [r1, r3, asl #2]
	.loc 2 294 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L36:
.LBE8:
	.loc 2 301 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L37
	.loc 2 302 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 301 0 discriminator 1
	cmp	r3, #6
	bgt	.L37
	.loc 2 303 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #4]
	.loc 2 304 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 302 0
	cmp	r2, r3
	bne	.L37
	.loc 2 305 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #4]
	.loc 2 306 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	.loc 2 304 0
	cmp	r2, r3
	bne	.L37
.LBB9:
	.loc 2 311 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #228]
	.loc 2 312 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	str	r3, [sp, #224]
	.loc 2 315 0
	ldr	r2, [sp, #224]
	ldr	r3, [sp, #228]
	cmp	r2, r3
	ble	.L37
	.loc 2 318 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 321 0
	ldr	r2, [sp, #228]
	ldr	r3, [sp, #224]
	add	r3, r2, r3
	str	r3, [sp, #220]
	.loc 2 324 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #228]
	str	r2, [r1, r3, asl #2]
	.loc 2 325 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #224]
	str	r2, [r1, r3, asl #2]
	.loc 2 326 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #220]
	str	r2, [r1, r3, asl #2]
	.loc 2 327 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L37:
.LBE9:
	.loc 2 334 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L38
	.loc 2 335 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 334 0 discriminator 1
	cmp	r3, #6
	bgt	.L38
	.loc 2 336 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #8]
	.loc 2 337 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	.loc 2 335 0
	cmp	r2, r3
	bne	.L38
	.loc 2 338 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #8]
	.loc 2 339 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 337 0
	cmp	r2, r3
	bne	.L38
.LBB10:
	.loc 2 344 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #216]
	.loc 2 345 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	str	r3, [sp, #212]
	.loc 2 348 0
	ldr	r2, [sp, #212]
	ldr	r3, [sp, #216]
	cmp	r2, r3
	ble	.L38
	.loc 2 351 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 354 0
	ldr	r2, [sp, #216]
	ldr	r3, [sp, #212]
	add	r3, r2, r3
	str	r3, [sp, #208]
	.loc 2 357 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #216]
	str	r2, [r1, r3, asl #2]
	.loc 2 358 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #212]
	str	r2, [r1, r3, asl #2]
	.loc 2 359 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #208]
	str	r2, [r1, r3, asl #2]
	.loc 2 360 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L38:
.LBE10:
	.loc 2 367 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L39
	.loc 2 368 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 367 0 discriminator 1
	cmp	r3, #6
	bgt	.L39
	.loc 2 369 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #8]
	.loc 2 370 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 368 0
	cmp	r2, r3
	bne	.L39
	.loc 2 371 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #8]
	.loc 2 372 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	.loc 2 370 0
	cmp	r2, r3
	bne	.L39
.LBB11:
	.loc 2 377 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #204]
	.loc 2 378 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	str	r3, [sp, #200]
	.loc 2 381 0
	ldr	r2, [sp, #200]
	ldr	r3, [sp, #204]
	cmp	r2, r3
	ble	.L39
	.loc 2 384 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 387 0
	ldr	r2, [sp, #204]
	ldr	r3, [sp, #200]
	add	r3, r2, r3
	str	r3, [sp, #196]
	.loc 2 390 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #204]
	str	r2, [r1, r3, asl #2]
	.loc 2 391 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #200]
	str	r2, [r1, r3, asl #2]
	.loc 2 392 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #196]
	str	r2, [r1, r3, asl #2]
	.loc 2 393 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L39:
.LBE11:
	.loc 2 400 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L40
	.loc 2 401 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 400 0 discriminator 1
	cmp	r3, #6
	bgt	.L40
	.loc 2 402 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 403 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	.loc 2 401 0
	cmp	r2, r3
	bne	.L40
	.loc 2 404 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 405 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	.loc 2 403 0
	cmp	r2, r3
	bne	.L40
.LBB12:
	.loc 2 410 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #192]
	.loc 2 411 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	str	r3, [sp, #188]
	.loc 2 414 0
	ldr	r2, [sp, #188]
	ldr	r3, [sp, #192]
	cmp	r2, r3
	ble	.L40
	.loc 2 417 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 420 0
	ldr	r2, [sp, #192]
	ldr	r3, [sp, #188]
	add	r3, r2, r3
	str	r3, [sp, #184]
	.loc 2 423 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #192]
	str	r2, [r1, r3, asl #2]
	.loc 2 424 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #188]
	str	r2, [r1, r3, asl #2]
	.loc 2 425 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #184]
	str	r2, [r1, r3, asl #2]
	.loc 2 426 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L40:
.LBE12:
	.loc 2 433 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L41
	.loc 2 434 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 433 0 discriminator 1
	cmp	r3, #6
	bgt	.L41
	.loc 2 435 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 436 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	.loc 2 434 0
	cmp	r2, r3
	bne	.L41
	.loc 2 437 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 438 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	.loc 2 436 0
	cmp	r2, r3
	bne	.L41
.LBB13:
	.loc 2 443 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #180]
	.loc 2 444 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	str	r3, [sp, #176]
	.loc 2 447 0
	ldr	r2, [sp, #176]
	ldr	r3, [sp, #180]
	cmp	r2, r3
	ble	.L41
	.loc 2 450 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 453 0
	ldr	r2, [sp, #180]
	ldr	r3, [sp, #176]
	add	r3, r2, r3
	str	r3, [sp, #172]
	.loc 2 456 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #180]
	str	r2, [r1, r3, asl #2]
	.loc 2 457 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #176]
	str	r2, [r1, r3, asl #2]
	.loc 2 458 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #172]
	str	r2, [r1, r3, asl #2]
	.loc 2 459 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L41:
.LBE13:
	.loc 2 466 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L42
	.loc 2 467 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 466 0 discriminator 1
	cmp	r3, #6
	bgt	.L42
	.loc 2 468 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 469 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	.loc 2 467 0
	cmp	r2, r3
	bne	.L42
	.loc 2 470 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 471 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 469 0
	cmp	r2, r3
	bne	.L42
.LBB14:
	.loc 2 476 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	str	r3, [sp, #168]
	.loc 2 477 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #164]
	.loc 2 480 0
	ldr	r2, [sp, #164]
	ldr	r3, [sp, #168]
	cmp	r2, r3
	ble	.L42
	.loc 2 483 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 486 0
	ldr	r2, [sp, #168]
	ldr	r3, [sp, #164]
	add	r3, r2, r3
	str	r3, [sp, #160]
	.loc 2 489 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #168]
	str	r2, [r1, r3, asl #2]
	.loc 2 490 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #164]
	str	r2, [r1, r3, asl #2]
	.loc 2 491 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #160]
	str	r2, [r1, r3, asl #2]
	.loc 2 492 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L42:
.LBE14:
	.loc 2 499 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L43
	.loc 2 500 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 499 0 discriminator 1
	cmp	r3, #6
	bgt	.L43
	.loc 2 501 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 502 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 500 0
	cmp	r2, r3
	bne	.L43
	.loc 2 503 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 504 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	.loc 2 502 0
	cmp	r2, r3
	bne	.L43
.LBB15:
	.loc 2 509 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	str	r3, [sp, #156]
	.loc 2 510 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #152]
	.loc 2 513 0
	ldr	r2, [sp, #152]
	ldr	r3, [sp, #156]
	cmp	r2, r3
	ble	.L43
	.loc 2 516 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 519 0
	ldr	r2, [sp, #156]
	ldr	r3, [sp, #152]
	add	r3, r2, r3
	str	r3, [sp, #148]
	.loc 2 522 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #156]
	str	r2, [r1, r3, asl #2]
	.loc 2 523 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #152]
	str	r2, [r1, r3, asl #2]
	.loc 2 524 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #148]
	str	r2, [r1, r3, asl #2]
	.loc 2 525 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L43:
.LBE15:
	.loc 2 532 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L44
	.loc 2 533 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 532 0 discriminator 1
	cmp	r3, #6
	bgt	.L44
	.loc 2 534 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #8]
	.loc 2 535 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	.loc 2 533 0
	cmp	r2, r3
	bne	.L44
	.loc 2 536 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #8]
	.loc 2 537 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 535 0
	cmp	r2, r3
	bne	.L44
.LBB16:
	.loc 2 542 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	str	r3, [sp, #144]
	.loc 2 543 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	str	r3, [sp, #140]
	.loc 2 546 0
	ldr	r2, [sp, #140]
	ldr	r3, [sp, #144]
	cmp	r2, r3
	ble	.L44
	.loc 2 549 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 552 0
	ldr	r2, [sp, #144]
	ldr	r3, [sp, #140]
	add	r3, r2, r3
	str	r3, [sp, #136]
	.loc 2 555 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #144]
	str	r2, [r1, r3, asl #2]
	.loc 2 556 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #140]
	str	r2, [r1, r3, asl #2]
	.loc 2 557 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #136]
	str	r2, [r1, r3, asl #2]
	.loc 2 558 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L44:
.LBE16:
	.loc 2 565 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L45
	.loc 2 566 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 565 0 discriminator 1
	cmp	r3, #6
	bgt	.L45
	.loc 2 567 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #8]
	.loc 2 568 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 566 0
	cmp	r2, r3
	bne	.L45
	.loc 2 569 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #8]
	.loc 2 570 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	.loc 2 568 0
	cmp	r2, r3
	bne	.L45
.LBB17:
	.loc 2 575 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	str	r3, [sp, #132]
	.loc 2 576 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	str	r3, [sp, #128]
	.loc 2 579 0
	ldr	r2, [sp, #128]
	ldr	r3, [sp, #132]
	cmp	r2, r3
	ble	.L45
	.loc 2 581 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 584 0
	ldr	r2, [sp, #132]
	ldr	r3, [sp, #128]
	add	r3, r2, r3
	str	r3, [sp, #124]
	.loc 2 587 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #132]
	str	r2, [r1, r3, asl #2]
	.loc 2 588 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #128]
	str	r2, [r1, r3, asl #2]
	.loc 2 589 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #124]
	str	r2, [r1, r3, asl #2]
	.loc 2 590 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L45:
.LBE17:
	.loc 2 597 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L46
	.loc 2 598 0 discriminator 1
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 597 0 discriminator 1
	cmp	r3, #6
	bgt	.L46
	.loc 2 599 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 600 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	.loc 2 598 0
	cmp	r2, r3
	bne	.L46
	.loc 2 601 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 602 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	.loc 2 600 0
	cmp	r2, r3
	bne	.L46
.LBB18:
	.loc 2 607 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	str	r3, [sp, #120]
	.loc 2 608 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	str	r3, [sp, #116]
	.loc 2 611 0
	ldr	r2, [sp, #116]
	ldr	r3, [sp, #120]
	cmp	r2, r3
	ble	.L46
	.loc 2 613 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 616 0
	ldr	r2, [sp, #120]
	ldr	r3, [sp, #116]
	add	r3, r2, r3
	str	r3, [sp, #112]
	.loc 2 619 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #120]
	str	r2, [r1, r3, asl #2]
	.loc 2 620 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #116]
	str	r2, [r1, r3, asl #2]
	.loc 2 621 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #112]
	str	r2, [r1, r3, asl #2]
	.loc 2 622 0
	ldr	r3, .L56+4
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+4
	str	r3, [r2]
.L46:
.LBE18:
	.loc 2 630 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L47
	b	.L57
.L58:
	.align	2
.L56:
	.word	petrinet_P1_is_marked
	.word	petrinet_P3_is_marked
	.word	petrinet_P1_marking_member_0
	.word	petrinet_P2_marking_member_0
	.word	petrinet_P2_is_marked
	.word	petrinet_P3_marking_member_0
	.word	petrinet_P3_is_marked
.L57:
	.loc 2 631 0 discriminator 1
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 630 0 discriminator 1
	cmp	r3, #6
	bgt	.L47
	.loc 2 632 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 633 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	.loc 2 631 0
	cmp	r2, r3
	bne	.L47
	.loc 2 634 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 635 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	.loc 2 633 0
	cmp	r2, r3
	bne	.L47
.LBB19:
	.loc 2 640 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	str	r3, [sp, #108]
	.loc 2 641 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	str	r3, [sp, #104]
	.loc 2 644 0
	ldr	r2, [sp, #104]
	ldr	r3, [sp, #108]
	cmp	r2, r3
	ble	.L47
	.loc 2 647 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 650 0
	ldr	r2, [sp, #108]
	ldr	r3, [sp, #104]
	add	r3, r2, r3
	str	r3, [sp, #100]
	.loc 2 653 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #108]
	str	r2, [r1, r3, asl #2]
	.loc 2 654 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #104]
	str	r2, [r1, r3, asl #2]
	.loc 2 655 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #100]
	str	r2, [r1, r3, asl #2]
	.loc 2 656 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+24
	str	r3, [r2]
.L47:
.LBE19:
	.loc 2 664 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L48
	.loc 2 665 0 discriminator 1
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 664 0 discriminator 1
	cmp	r3, #6
	bgt	.L48
	.loc 2 666 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 667 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	.loc 2 665 0
	cmp	r2, r3
	bne	.L48
	.loc 2 668 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 669 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 667 0
	cmp	r2, r3
	bne	.L48
.LBB20:
	.loc 2 674 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	str	r3, [sp, #96]
	.loc 2 675 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #92]
	.loc 2 678 0
	ldr	r2, [sp, #92]
	ldr	r3, [sp, #96]
	cmp	r2, r3
	ble	.L48
	.loc 2 680 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 683 0
	ldr	r2, [sp, #96]
	ldr	r3, [sp, #92]
	add	r3, r2, r3
	str	r3, [sp, #88]
	.loc 2 686 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #96]
	str	r2, [r1, r3, asl #2]
	.loc 2 687 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #92]
	str	r2, [r1, r3, asl #2]
	.loc 2 688 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #88]
	str	r2, [r1, r3, asl #2]
	.loc 2 689 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+24
	str	r3, [r2]
.L48:
.LBE20:
	.loc 2 696 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L49
	.loc 2 697 0 discriminator 1
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 696 0 discriminator 1
	cmp	r3, #6
	bgt	.L49
	.loc 2 698 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 699 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 697 0
	cmp	r2, r3
	bne	.L49
	.loc 2 700 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 701 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	.loc 2 699 0
	cmp	r2, r3
	bne	.L49
.LBB21:
	.loc 2 706 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	str	r3, [sp, #84]
	.loc 2 707 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #80]
	.loc 2 710 0
	ldr	r2, [sp, #80]
	ldr	r3, [sp, #84]
	cmp	r2, r3
	ble	.L49
	.loc 2 712 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 715 0
	ldr	r2, [sp, #84]
	ldr	r3, [sp, #80]
	add	r3, r2, r3
	str	r3, [sp, #76]
	.loc 2 718 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #84]
	str	r2, [r1, r3, asl #2]
	.loc 2 719 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #80]
	str	r2, [r1, r3, asl #2]
	.loc 2 720 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #76]
	str	r2, [r1, r3, asl #2]
	.loc 2 721 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+24
	str	r3, [r2]
.L49:
.LBE21:
	.loc 2 728 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L50
	.loc 2 729 0 discriminator 1
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 728 0 discriminator 1
	cmp	r3, #6
	bgt	.L50
	.loc 2 730 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #4]
	.loc 2 731 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	.loc 2 729 0
	cmp	r2, r3
	bne	.L50
	.loc 2 732 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #4]
	.loc 2 733 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 731 0
	cmp	r2, r3
	bne	.L50
.LBB22:
	.loc 2 738 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	str	r3, [sp, #72]
	.loc 2 739 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	str	r3, [sp, #68]
	.loc 2 742 0
	ldr	r2, [sp, #68]
	ldr	r3, [sp, #72]
	cmp	r2, r3
	ble	.L50
	.loc 2 744 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 747 0
	ldr	r2, [sp, #72]
	ldr	r3, [sp, #68]
	add	r3, r2, r3
	str	r3, [sp, #64]
	.loc 2 750 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #72]
	str	r2, [r1, r3, asl #2]
	.loc 2 751 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #68]
	str	r2, [r1, r3, asl #2]
	.loc 2 752 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #64]
	str	r2, [r1, r3, asl #2]
	.loc 2 753 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+24
	str	r3, [r2]
.L50:
.LBE22:
	.loc 2 760 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L51
	.loc 2 761 0 discriminator 1
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 760 0 discriminator 1
	cmp	r3, #6
	bgt	.L51
	.loc 2 762 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #4]
	.loc 2 763 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	.loc 2 761 0
	cmp	r2, r3
	bne	.L51
	.loc 2 764 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #4]
	.loc 2 765 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	.loc 2 763 0
	cmp	r2, r3
	bne	.L51
.LBB23:
	.loc 2 770 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	str	r3, [sp, #60]
	.loc 2 771 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	str	r3, [sp, #56]
	.loc 2 774 0
	ldr	r2, [sp, #56]
	ldr	r3, [sp, #60]
	cmp	r2, r3
	ble	.L51
	.loc 2 776 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 779 0
	ldr	r2, [sp, #60]
	ldr	r3, [sp, #56]
	add	r3, r2, r3
	str	r3, [sp, #52]
	.loc 2 782 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #60]
	str	r2, [r1, r3, asl #2]
	.loc 2 783 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #56]
	str	r2, [r1, r3, asl #2]
	.loc 2 784 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #52]
	str	r2, [r1, r3, asl #2]
	.loc 2 785 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+24
	str	r3, [r2]
.L51:
.LBE23:
	.loc 2 792 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L52
	.loc 2 793 0 discriminator 1
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 792 0 discriminator 1
	cmp	r3, #6
	bgt	.L52
	.loc 2 794 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 795 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	.loc 2 793 0
	cmp	r2, r3
	bne	.L52
	.loc 2 796 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 797 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	.loc 2 795 0
	cmp	r2, r3
	bne	.L52
.LBB24:
	.loc 2 802 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	str	r3, [sp, #48]
	.loc 2 803 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	str	r3, [sp, #44]
	.loc 2 806 0
	ldr	r2, [sp, #44]
	ldr	r3, [sp, #48]
	cmp	r2, r3
	ble	.L52
	.loc 2 808 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 811 0
	ldr	r2, [sp, #48]
	ldr	r3, [sp, #44]
	add	r3, r2, r3
	str	r3, [sp, #40]
	.loc 2 814 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #48]
	str	r2, [r1, r3, asl #2]
	.loc 2 815 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #44]
	str	r2, [r1, r3, asl #2]
	.loc 2 816 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #40]
	str	r2, [r1, r3, asl #2]
	.loc 2 817 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+24
	str	r3, [r2]
.L52:
.LBE24:
	.loc 2 824 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L53
	.loc 2 825 0 discriminator 1
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 824 0 discriminator 1
	cmp	r3, #6
	bgt	.L53
	.loc 2 826 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 827 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	.loc 2 825 0
	cmp	r2, r3
	bne	.L53
	.loc 2 828 0
	ldr	r3, .L56+12
	ldr	r2, [r3, #12]
	.loc 2 829 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	.loc 2 827 0
	cmp	r2, r3
	bne	.L53
.LBB25:
	.loc 2 834 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	str	r3, [sp, #36]
	.loc 2 835 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	str	r3, [sp, #32]
	.loc 2 838 0
	ldr	r2, [sp, #32]
	ldr	r3, [sp, #36]
	cmp	r2, r3
	ble	.L53
	.loc 2 840 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 843 0
	ldr	r2, [sp, #36]
	ldr	r3, [sp, #32]
	add	r3, r2, r3
	str	r3, [sp, #28]
	.loc 2 846 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #36]
	str	r2, [r1, r3, asl #2]
	.loc 2 847 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #32]
	str	r2, [r1, r3, asl #2]
	.loc 2 848 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #28]
	str	r2, [r1, r3, asl #2]
	.loc 2 849 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+24
	str	r3, [r2]
.L53:
.LBE25:
	.loc 2 856 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L54
	.loc 2 857 0 discriminator 1
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 856 0 discriminator 1
	cmp	r3, #6
	bgt	.L54
	.loc 2 858 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 859 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	.loc 2 857 0
	cmp	r2, r3
	bne	.L54
	.loc 2 860 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 861 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	.loc 2 859 0
	cmp	r2, r3
	bne	.L54
.LBB26:
	.loc 2 866 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	str	r3, [sp, #24]
	.loc 2 867 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #20]
	.loc 2 870 0
	ldr	r2, [sp, #20]
	ldr	r3, [sp, #24]
	cmp	r2, r3
	ble	.L54
	.loc 2 873 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 876 0
	ldr	r2, [sp, #24]
	ldr	r3, [sp, #20]
	add	r3, r2, r3
	str	r3, [sp, #16]
	.loc 2 879 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #24]
	str	r2, [r1, r3, asl #2]
	.loc 2 880 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #20]
	str	r2, [r1, r3, asl #2]
	.loc 2 881 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #16]
	str	r2, [r1, r3, asl #2]
	.loc 2 882 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+24
	str	r3, [r2]
.L54:
.LBE26:
	.loc 2 890 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	cmp	r3, #3
	ble	.L29
	.loc 2 891 0 discriminator 1
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	.loc 2 890 0 discriminator 1
	cmp	r3, #6
	bgt	.L29
	.loc 2 892 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 893 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #8]
	.loc 2 891 0
	cmp	r2, r3
	bne	.L29
	.loc 2 894 0
	ldr	r3, .L56+12
	ldr	r2, [r3]
	.loc 2 895 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #4]
	.loc 2 893 0
	cmp	r2, r3
	bne	.L29
.LBB27:
	.loc 2 901 0
	ldr	r3, .L56+12
	ldr	r3, [r3, #12]
	str	r3, [sp, #12]
	.loc 2 902 0
	ldr	r3, .L56+12
	ldr	r3, [r3]
	str	r3, [sp, #8]
	.loc 2 905 0
	ldr	r2, [sp, #8]
	ldr	r3, [sp, #12]
	cmp	r2, r3
	ble	.L29
	.loc 2 908 0
	ldr	r3, .L56+16
	ldr	r3, [r3]
	sub	r3, r3, #4
	ldr	r2, .L56+16
	str	r3, [r2]
	.loc 2 911 0
	ldr	r2, [sp, #12]
	ldr	r3, [sp, #8]
	add	r3, r2, r3
	str	r3, [sp, #4]
	.loc 2 914 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	ldr	r1, .L56+20
	ldr	r2, [sp, #12]
	str	r2, [r1, r3, asl #2]
	.loc 2 915 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #1
	ldr	r1, .L56+20
	ldr	r2, [sp, #8]
	str	r2, [r1, r3, asl #2]
	.loc 2 916 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #2
	ldr	r1, .L56+20
	ldr	r2, [sp, #4]
	str	r2, [r1, r3, asl #2]
	.loc 2 917 0
	ldr	r3, .L56+24
	ldr	r3, [r3]
	add	r3, r3, #3
	ldr	r2, .L56+24
	str	r3, [r2]
.L29:
.LBE27:
	.loc 2 68 0
	ldr	r3, [sp, #316]
	cmp	r3, #0
	bgt	.L55
	.loc 2 937 0
	mov	r0, r0	@ nop
	add	sp, sp, #320
	.cfi_def_cfa_offset 0
	@ sp needed
	bx	lr
	.cfi_endproc
.LFE27:
	.size	petrinet_main, .-petrinet_main
	.align	2
	.global	petrinet_init
	.type	petrinet_init, %function
petrinet_init:
.LFB28:
	.loc 2 941 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 2 942 0
	ldr	r3, .L60
	mov	r2, #3
	str	r2, [r3]
	.loc 2 943 0
	ldr	r3, .L60+4
	mov	r2, #5
	str	r2, [r3]
	.loc 2 944 0
	ldr	r3, .L60+8
	mov	r2, #0
	str	r2, [r3]
	.loc 2 955 0
	mov	r0, r0	@ nop
	bx	lr
.L61:
	.align	2
.L60:
	.word	petrinet_P1_is_marked
	.word	petrinet_P2_is_marked
	.word	petrinet_P3_is_marked
	.cfi_endproc
.LFE28:
	.size	petrinet_init, .-petrinet_init
	.align	2
	.global	petrinet_return
	.type	petrinet_return, %function
petrinet_return:
.LFB29:
	.loc 2 959 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #8
	.cfi_def_cfa_offset 8
	.loc 2 961 0
	mov	r3, #0
	str	r3, [sp, #4]
	.loc 2 965 0
	mov	r3, #0
	str	r3, [sp]
	b	.L63
.L64:
	.loc 2 966 0 discriminator 3
	.syntax divided
@ 966 "X_BENCH.c" 1
	1:
        .section .wcet_annot
        .long 1b
        .long 1
        .long 3
                        .text
@ 0 "" 2
	.loc 2 968 0 discriminator 3
	.arm
	.syntax divided
	ldr	r2, .L72
	ldr	r3, [sp]
	ldr	r3, [r2, r3, asl #2]
	ldr	r2, [sp, #4]
	add	r3, r2, r3
	str	r3, [sp, #4]
	.loc 2 965 0 discriminator 3
	ldr	r3, [sp]
	add	r3, r3, #1
	str	r3, [sp]
.L63:
	.loc 2 965 0 is_stmt 0 discriminator 1
	ldr	r3, [sp]
	cmp	r3, #2
	ble	.L64
	.loc 2 971 0 is_stmt 1
	mov	r3, #0
	str	r3, [sp]
	b	.L65
.L66:
	.loc 2 972 0 discriminator 3
	.syntax divided
@ 972 "X_BENCH.c" 1
	1:
        .section .wcet_annot
        .long 1b
        .long 1
        .long 5
                        .text
@ 0 "" 2
	.loc 2 973 0 discriminator 3
	.arm
	.syntax divided
	ldr	r2, .L72+4
	ldr	r3, [sp]
	ldr	r3, [r2, r3, asl #2]
	ldr	r2, [sp, #4]
	add	r3, r2, r3
	str	r3, [sp, #4]
	.loc 2 971 0 discriminator 3
	ldr	r3, [sp]
	add	r3, r3, #1
	str	r3, [sp]
.L65:
	.loc 2 971 0 is_stmt 0 discriminator 1
	ldr	r3, [sp]
	cmp	r3, #4
	ble	.L66
	.loc 2 976 0 is_stmt 1
	mov	r3, #0
	str	r3, [sp]
	b	.L67
.L68:
	.loc 2 977 0 discriminator 3
	.syntax divided
@ 977 "X_BENCH.c" 1
	1:
        .section .wcet_annot
        .long 1b
        .long 1
        .long 6
                        .text
@ 0 "" 2
	.loc 2 978 0 discriminator 3
	.arm
	.syntax divided
	ldr	r2, .L72+8
	ldr	r3, [sp]
	ldr	r3, [r2, r3, asl #2]
	ldr	r2, [sp, #4]
	add	r3, r2, r3
	str	r3, [sp, #4]
	.loc 2 976 0 discriminator 3
	ldr	r3, [sp]
	add	r3, r3, #1
	str	r3, [sp]
.L67:
	.loc 2 976 0 is_stmt 0 discriminator 1
	ldr	r3, [sp]
	cmp	r3, #5
	ble	.L68
	.loc 2 981 0 is_stmt 1
	mov	r2, #0
	ldr	r3, [sp, #4]
	cmp	r3, r2
	bne	.L69
	.loc 2 981 0 is_stmt 0 discriminator 1
	mov	r3, #0
	b	.L71
.L69:
	.loc 2 981 0 discriminator 2
	mvn	r3, #0
.L71:
	.loc 2 982 0 is_stmt 1 discriminator 5
	mov	r0, r3
	add	sp, sp, #8
	.cfi_def_cfa_offset 0
	@ sp needed
	bx	lr
.L73:
	.align	2
.L72:
	.word	petrinet_P1_marking_member_0
	.word	petrinet_P2_marking_member_0
	.word	petrinet_P3_marking_member_0
	.cfi_endproc
.LFE29:
	.size	petrinet_return, .-petrinet_return
	.align	2
	.global	main
	.type	main, %function
main:
.LFB30:
	.loc 2 986 0
	.cfi_startproc
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 4, -8
	.cfi_offset 14, -4
	.loc 2 987 0
	bl	petrinet_main
	.loc 2 989 0
	bl	petrinet_return
	mov	r3, r0
	.loc 2 990 0
	mov	r0, r3
	ldmfd	sp!, {r4, lr}
	.cfi_restore 14
	.cfi_restore 4
	.cfi_def_cfa_offset 0
	bx	lr
	.cfi_endproc
.LFE30:
	.size	main, .-main
.Letext0:
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x8d5
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF38
	.byte	0xc
	.4byte	.LASF39
	.4byte	.LASF40
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x1d
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x1e
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0x1f
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.byte	0x20
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x21
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x25
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.byte	0x26
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.byte	0x27
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.byte	0x28
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.byte	0x29
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.byte	0x2a
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.byte	0x2b
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF12
	.byte	0x1
	.byte	0x2c
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x1
	.byte	0x2d
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.byte	0x2e
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x1
	.byte	0x31
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x1
	.byte	0x32
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x1
	.byte	0x33
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF18
	.byte	0x1
	.byte	0x34
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x1
	.byte	0x35
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF20
	.byte	0x1
	.byte	0x36
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF21
	.byte	0x1
	.byte	0x37
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF22
	.byte	0x1
	.byte	0x38
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF23
	.byte	0x1
	.byte	0x3b
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF24
	.byte	0x1
	.byte	0x3c
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF25
	.byte	0x1
	.byte	0x3e
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2
	.4byte	.LASF26
	.byte	0x1
	.byte	0x3f
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x3
	.4byte	.LASF41
	.byte	0x2
	.byte	0x3a
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x79f
	.uleb128 0x4
	.4byte	.LASF27
	.byte	0x2
	.byte	0x3c
	.4byte	0x79f
	.uleb128 0x2
	.byte	0x91
	.sleb128 -4
	.uleb128 0x5
	.4byte	.LBB2
	.4byte	.LBE2-.LBB2
	.4byte	0x245
	.uleb128 0x6
	.ascii	"x\000"
	.byte	0x2
	.byte	0x53
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -8
	.uleb128 0x6
	.ascii	"y\000"
	.byte	0x2
	.byte	0x54
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x6
	.ascii	"z\000"
	.byte	0x2
	.byte	0x55
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x5
	.4byte	.LBB3
	.4byte	.LBE3-.LBB3
	.4byte	0x277
	.uleb128 0x6
	.ascii	"x\000"
	.byte	0x2
	.byte	0x73
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x6
	.ascii	"y\000"
	.byte	0x2
	.byte	0x74
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x6
	.ascii	"z\000"
	.byte	0x2
	.byte	0x75
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.byte	0
	.uleb128 0x5
	.4byte	.LBB4
	.4byte	.LBE4-.LBB4
	.4byte	0x2a9
	.uleb128 0x6
	.ascii	"x\000"
	.byte	0x2
	.byte	0x93
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x6
	.ascii	"y\000"
	.byte	0x2
	.byte	0x94
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x6
	.ascii	"z\000"
	.byte	0x2
	.byte	0x95
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -40
	.byte	0
	.uleb128 0x5
	.4byte	.LBB5
	.4byte	.LBE5-.LBB5
	.4byte	0x2db
	.uleb128 0x6
	.ascii	"x\000"
	.byte	0x2
	.byte	0xb3
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x6
	.ascii	"y\000"
	.byte	0x2
	.byte	0xb4
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x6
	.ascii	"z\000"
	.byte	0x2
	.byte	0xb5
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -52
	.byte	0
	.uleb128 0x5
	.4byte	.LBB6
	.4byte	.LBE6-.LBB6
	.4byte	0x30d
	.uleb128 0x6
	.ascii	"x\000"
	.byte	0x2
	.byte	0xd2
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x6
	.ascii	"y\000"
	.byte	0x2
	.byte	0xd3
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x6
	.ascii	"z\000"
	.byte	0x2
	.byte	0xd4
	.4byte	0x7a6
	.uleb128 0x2
	.byte	0x91
	.sleb128 -64
	.byte	0
	.uleb128 0x5
	.4byte	.LBB7
	.4byte	.LBE7-.LBB7
	.4byte	0x342
	.uleb128 0x6
	.ascii	"x\000"
	.byte	0x2
	.byte	0xf1
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x6
	.ascii	"y\000"
	.byte	0x2
	.byte	0xf2
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x6
	.ascii	"z\000"
	.byte	0x2
	.byte	0xf3
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -76
	.byte	0
	.uleb128 0x5
	.4byte	.LBB8
	.4byte	.LBE8-.LBB8
	.4byte	0x37a
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x112
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x113
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x114
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -88
	.byte	0
	.uleb128 0x5
	.4byte	.LBB9
	.4byte	.LBE9-.LBB9
	.4byte	0x3b2
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x133
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -92
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x134
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x135
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -100
	.byte	0
	.uleb128 0x5
	.4byte	.LBB10
	.4byte	.LBE10-.LBB10
	.4byte	0x3ea
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x154
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -104
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x155
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -108
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x156
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -112
	.byte	0
	.uleb128 0x5
	.4byte	.LBB11
	.4byte	.LBE11-.LBB11
	.4byte	0x422
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x175
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -116
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x176
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -120
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x177
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -124
	.byte	0
	.uleb128 0x5
	.4byte	.LBB12
	.4byte	.LBE12-.LBB12
	.4byte	0x45a
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x196
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -128
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x197
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -132
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x198
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -136
	.byte	0
	.uleb128 0x5
	.4byte	.LBB13
	.4byte	.LBE13-.LBB13
	.4byte	0x492
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x1b7
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -140
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x1b8
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -144
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x1b9
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -148
	.byte	0
	.uleb128 0x5
	.4byte	.LBB14
	.4byte	.LBE14-.LBB14
	.4byte	0x4ca
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x1d8
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -152
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x1d9
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -156
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x1da
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -160
	.byte	0
	.uleb128 0x5
	.4byte	.LBB15
	.4byte	.LBE15-.LBB15
	.4byte	0x502
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x1f9
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -164
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x1fa
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -168
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x1fb
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -172
	.byte	0
	.uleb128 0x5
	.4byte	.LBB16
	.4byte	.LBE16-.LBB16
	.4byte	0x53a
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x21a
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -176
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x21b
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -180
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x21c
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -184
	.byte	0
	.uleb128 0x5
	.4byte	.LBB17
	.4byte	.LBE17-.LBB17
	.4byte	0x572
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x23b
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -188
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x23c
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -192
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x23d
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -196
	.byte	0
	.uleb128 0x5
	.4byte	.LBB18
	.4byte	.LBE18-.LBB18
	.4byte	0x5aa
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x25b
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -200
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x25c
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -204
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x25d
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -208
	.byte	0
	.uleb128 0x5
	.4byte	.LBB19
	.4byte	.LBE19-.LBB19
	.4byte	0x5e2
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x27c
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -212
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x27d
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -216
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x27e
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -220
	.byte	0
	.uleb128 0x5
	.4byte	.LBB20
	.4byte	.LBE20-.LBB20
	.4byte	0x61a
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x29e
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -224
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x29f
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -228
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x2a0
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -232
	.byte	0
	.uleb128 0x5
	.4byte	.LBB21
	.4byte	.LBE21-.LBB21
	.4byte	0x652
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x2be
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -236
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x2bf
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -240
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x2c0
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -244
	.byte	0
	.uleb128 0x5
	.4byte	.LBB22
	.4byte	.LBE22-.LBB22
	.4byte	0x68a
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x2de
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -248
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x2df
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -252
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x2e0
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -256
	.byte	0
	.uleb128 0x5
	.4byte	.LBB23
	.4byte	.LBE23-.LBB23
	.4byte	0x6c2
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x2fe
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -260
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x2ff
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -264
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x300
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -268
	.byte	0
	.uleb128 0x5
	.4byte	.LBB24
	.4byte	.LBE24-.LBB24
	.4byte	0x6fa
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x31e
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -272
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x31f
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -276
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x320
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -280
	.byte	0
	.uleb128 0x5
	.4byte	.LBB25
	.4byte	.LBE25-.LBB25
	.4byte	0x732
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x33e
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -284
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x33f
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -288
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x340
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -292
	.byte	0
	.uleb128 0x5
	.4byte	.LBB26
	.4byte	.LBE26-.LBB26
	.4byte	0x76a
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x35e
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -296
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x35f
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -300
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x360
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -304
	.byte	0
	.uleb128 0x8
	.4byte	.LBB27
	.4byte	.LBE27-.LBB27
	.uleb128 0x7
	.ascii	"a\000"
	.byte	0x2
	.2byte	0x381
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -308
	.uleb128 0x7
	.ascii	"b\000"
	.byte	0x2
	.2byte	0x382
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -312
	.uleb128 0x7
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x383
	.4byte	0x7a6
	.uleb128 0x3
	.byte	0x91
	.sleb128 -316
	.byte	0
	.byte	0
	.uleb128 0x9
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0xa
	.byte	0x4
	.byte	0x5
	.4byte	.LASF28
	.uleb128 0xb
	.4byte	.LASF42
	.byte	0x2
	.2byte	0x3ac
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xc
	.4byte	.LASF43
	.byte	0x2
	.2byte	0x3be
	.4byte	0x79f
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x7f6
	.uleb128 0xd
	.4byte	.LASF29
	.byte	0x2
	.2byte	0x3c1
	.4byte	0x79f
	.uleb128 0x2
	.byte	0x91
	.sleb128 -4
	.uleb128 0x7
	.ascii	"i\000"
	.byte	0x2
	.2byte	0x3c2
	.4byte	0x79f
	.uleb128 0x2
	.byte	0x91
	.sleb128 -8
	.byte	0
	.uleb128 0xe
	.4byte	.LASF44
	.byte	0x2
	.2byte	0x3d9
	.4byte	0x79f
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x2
	.byte	0x31
	.4byte	0x81d
	.uleb128 0x5
	.byte	0x3
	.4byte	petrinet_P1_is_marked
	.uleb128 0x10
	.4byte	0x79f
	.uleb128 0x11
	.4byte	0x839
	.4byte	0x832
	.uleb128 0x12
	.4byte	0x832
	.byte	0x2
	.byte	0
	.uleb128 0xa
	.byte	0x4
	.byte	0x7
	.4byte	.LASF30
	.uleb128 0x10
	.4byte	0x7a6
	.uleb128 0xf
	.4byte	.LASF32
	.byte	0x2
	.byte	0x32
	.4byte	0x84f
	.uleb128 0x5
	.byte	0x3
	.4byte	petrinet_P1_marking_member_0
	.uleb128 0x10
	.4byte	0x822
	.uleb128 0xf
	.4byte	.LASF33
	.byte	0x2
	.byte	0x33
	.4byte	0x81d
	.uleb128 0x5
	.byte	0x3
	.4byte	petrinet_P2_is_marked
	.uleb128 0x11
	.4byte	0x839
	.4byte	0x875
	.uleb128 0x12
	.4byte	0x832
	.byte	0x4
	.byte	0
	.uleb128 0xf
	.4byte	.LASF34
	.byte	0x2
	.byte	0x34
	.4byte	0x886
	.uleb128 0x5
	.byte	0x3
	.4byte	petrinet_P2_marking_member_0
	.uleb128 0x10
	.4byte	0x865
	.uleb128 0xf
	.4byte	.LASF35
	.byte	0x2
	.byte	0x35
	.4byte	0x81d
	.uleb128 0x5
	.byte	0x3
	.4byte	petrinet_P3_is_marked
	.uleb128 0x11
	.4byte	0x839
	.4byte	0x8ac
	.uleb128 0x12
	.4byte	0x832
	.byte	0x5
	.byte	0
	.uleb128 0xf
	.4byte	.LASF36
	.byte	0x2
	.byte	0x36
	.4byte	0x8bd
	.uleb128 0x5
	.byte	0x3
	.4byte	petrinet_P3_marking_member_0
	.uleb128 0x10
	.4byte	0x89c
	.uleb128 0xf
	.4byte	.LASF37
	.byte	0x2
	.byte	0x38
	.4byte	0x8d3
	.uleb128 0x5
	.byte	0x3
	.4byte	petrinet_CHECKSUM
	.uleb128 0x13
	.4byte	0x7a6
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF43:
	.ascii	"petrinet_return\000"
.LASF42:
	.ascii	"petrinet_init\000"
.LASF22:
	.ascii	"__aeabi_fadd\000"
.LASF8:
	.ascii	"__aeabi_dcmpge\000"
.LASF28:
	.ascii	"long int\000"
.LASF31:
	.ascii	"petrinet_P1_is_marked\000"
.LASF11:
	.ascii	"__aeabi_dcmple\000"
.LASF9:
	.ascii	"__aeabi_dcmpeq\000"
.LASF21:
	.ascii	"__aeabi_f2d\000"
.LASF27:
	.ascii	"dummy_i\000"
.LASF40:
	.ascii	"/home/paranwin/pi/heptane_svn2/heptane_svn/tmp\000"
.LASF39:
	.ascii	"/tmp/X_BENCH.c\000"
.LASF1:
	.ascii	"__aeabi_i2d\000"
.LASF4:
	.ascii	"__aeabi_i2f\000"
.LASF10:
	.ascii	"__aeabi_dcmpgt\000"
.LASF37:
	.ascii	"petrinet_CHECKSUM\000"
.LASF29:
	.ascii	"checksum\000"
.LASF32:
	.ascii	"petrinet_P1_marking_member_0\000"
.LASF19:
	.ascii	"__aeabi_fmul\000"
.LASF35:
	.ascii	"petrinet_P3_is_marked\000"
.LASF26:
	.ascii	"__aeabi_dcmplt\000"
.LASF41:
	.ascii	"petrinet_main\000"
.LASF15:
	.ascii	"__aeabi_fcmplt\000"
.LASF44:
	.ascii	"main\000"
.LASF12:
	.ascii	"__aeabi_d2f\000"
.LASF5:
	.ascii	"__aeabi_ddiv\000"
.LASF7:
	.ascii	"__aeabi_dmul\000"
.LASF20:
	.ascii	"__aeabi_fsub\000"
.LASF0:
	.ascii	"__aeabi_idiv\000"
.LASF25:
	.ascii	"__aeabi_dcmpun\000"
.LASF33:
	.ascii	"petrinet_P2_is_marked\000"
.LASF34:
	.ascii	"petrinet_P2_marking_member_0\000"
.LASF38:
	.ascii	"GNU C11 5.3.1 20160307 (release) [ARM/embedded-5-br"
	.ascii	"anch revision 234589] -mcpu=arm7tdmi -march=armv4t "
	.ascii	"-marm -ggdb -O0 -fomit-frame-pointer\000"
.LASF13:
	.ascii	"__aeabi_dadd\000"
.LASF24:
	.ascii	"__aeabi_ui2d\000"
.LASF3:
	.ascii	"__aeabi_fcmpge\000"
.LASF30:
	.ascii	"sizetype\000"
.LASF14:
	.ascii	"__aeabi_d2iz\000"
.LASF2:
	.ascii	"__aeabi_uidivmod\000"
.LASF16:
	.ascii	"__aeabi_fcmpgt\000"
.LASF18:
	.ascii	"__aeabi_fcmpeq\000"
.LASF6:
	.ascii	"__aeabi_dsub\000"
.LASF36:
	.ascii	"petrinet_P3_marking_member_0\000"
.LASF17:
	.ascii	"__aeabi_fdiv\000"
.LASF23:
	.ascii	"__aeabi_ui2\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 5.3.1 20160307 (release) [ARM/embedded-5-branch revision 234589]"
