#!/usr/bin/env bash

set -a

GREEN='\e[1m\e[32m'
RED='\e[1m\e[31m'
DEFAULT='\e[0m\e[39m'

if [[ "$#" -ne 2 ]]; then
    echo "Usage: run.sh BENCH_DIR BENCHMARK_C_FILE"
    exit
fi

[ -z "$TIMING_ML" ] &&
echo "You need to set TIMING_ML to the path of find_wcet.py. For instance:" &&
echo "export TIMING_ML=~/scripts/find_wcet.py" &&
exit 1;

BENCH_DIR=$1
C_FILE=$2

echo "C_FILE: $C_FILE"

rm -rf tmp
mkdir tmp

BENCH_NAME="X_BENCH"
TMP_DIR=$(readlink -f tmp/)

SUBST="\$BENCH_NAME:\$TMP_DIR:\$CROSS_COMPILE"

cp ${BENCH_DIR}/${C_FILE} tmp/${BENCH_NAME}.c
cp ${BENCH_DIR}/*.h tmp/
cp ${BENCH_DIR}/*.c tmp/
cp benchmarks/annot.h tmp/
cp benchmarks/ARMlib.h tmp/

cat config_files/configExtract_template_ARM.xml | envsubst ${SUBST} > tmp/extract.xml
cat config_files/configWCET_template_ARM.xml | envsubst  ${SUBST} > tmp/analysis.xml

EXTRACT=$(readlink -f $(find . -executable -type f -name HeptaneExtract))
ANALYSIS=$(readlink -f $(find . -executable -type f -name HeptaneAnalysis))

echo "Extract: $EXTRACT"
echo "Analysis: $ANALYSIS"

cd tmp

echo -e "${GREEN}Launching Extract${DEFAULT}"
${EXTRACT} extract.xml | sed 's/^/     /'

echo -e "${GREEN}Launching Analysis${DEFAULT}"
${ANALYSIS} analysis.xml | sed 's/^/     /'

echo -e "done"
cd ..
