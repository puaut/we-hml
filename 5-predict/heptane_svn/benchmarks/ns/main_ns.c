#include <annot.h>
#include "stdio.h"
#define main the_code
#include <toto.h>
#undef main


__attribute__((always_inline)) inline void ccnt_read(volatile unsigned* ptr)
{
	asm volatile ("mrc p15, 0, %0, c9, c13, 0" : "=r" (*ptr));
}

int main()
{
    volatile unsigned before, after;
    ccnt_read(&before);

    the_code();

    ccnt_read(&after);
    if (before > after) //printf("Counter overflow\n");
    //printf("%u\n", after - before);
    return 0;
}
