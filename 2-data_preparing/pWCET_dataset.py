import math
import os
import skextremes as ske
import matplotlib.pyplot as plt
import time
import statsmodels.tsa.stattools
import hurst
from pyextremes import EVA, __version__
import numpy as np
import pandas as pd

import pickle
import json

import numpy as np
import pandas as pd
import seaborn as sns

# List all files in a directory using scandir()
list_pollution = ['res_200_7500/'+str(2**i)+'/' for i in range(10)]
with open("global_instructions.pickle", "rb") as file:
    global_instructions = pickle.load(file)
global_instructions.append('pollution')
global_instructions.append('nb_instruction')
global_instructions.append('pWCET')
global_instructions.append('ratiopWCET')
i = 1
#list_XY = []
list_pollution = ['res_200_7500/'+str(0)+'/' ]
for pollution_folder in list_pollution:
    list_XY = []
    data = {}
    basepath = pollution_folder
    with os.scandir(basepath) as entries:
        for entry in entries:
            if entry.is_file():
                #print(entry.name)
                data[entry.name] = []
                # Using readlines()
                file1 = open(basepath + entry.name, 'r')
                Lines = file1.readlines()

                count = 0
                # Strips the newline character
                for line in Lines:
                    l = line.split(";")
                    if 'TIMINGML' in l[0] or 'Module loaded\n' in l[0] or 'rcu:' in l[0] or 'rcu_sched' in l[0] or '(__schedule)' in l[0] or '(schedule)' in l[0] or '(schedule_timeout)' in l[0] or '(rcu_gp_kthread)' in l[0] or 'kthread' in l[0] or 'Exception' in l[0]:
                        continue
                    else:
                        try:
                            sample = int(int(''.join(filter(str.isdigit, l[2]))))
                        except:
                            continue
                        data[entry.name].append(sample)


#index = [key for key in data]
#columns = ['KPSS_test','p_value_KPSS','BDS_test','p_value_BDS','H index','C']
#df = pd.DataFrame (index=index,columns=columns)

    for key in data:
        evt = data[key]
        # plt.hist(evt)
        if data[key] == []:
            continue
        print(key)
        #print(data[key])
        #print("here")
        """ First test Stationnary"""
        try:
            kpss_stat, p_value_KPSS, lags, crit = statsmodels.tsa.stattools.kpss(evt, regression='c', nlags=None, store=False)
        except:
            print("error kpss ", key)
            print(data[key])
            kpss_stat = -1
            p_value_KPSS = -1

        """ Second test Short indep"""
        try:
            bds_stat, p_value_BDS = statsmodels.tsa.stattools.bds(evt)
        except:
            print("error BDS ", key)
            print(data[key])
            bds_stat = -1
            p_value_BDS = -1

        """ Third test long indep"""
        try:
            H, c, dat = hurst.compute_Hc(evt, kind='price', simplified=True)
        except:
            print("error H index ", key)
            print(data[key])
            H = -1
            c = -1

        if ( p_value_KPSS >= 0.01 and
             p_value_BDS  >= 0.01 and
             H <= 0.5 + math.exp(-7.51*math.log(math.log(math.log2(len(evt))))+4.58) and
             H >= 0.5 - math.exp(-7.19*math.log(math.log(math.log2(len(evt))))+4.34)):
#         if 0 == 0 :
            features = key.split('-')[0]+'.json'
            with open('res_200_7500/'+features) as file:
                instructions_dict = json.load(file)
            X = {}
            for xi in global_instructions:
                X[xi] = 0.0001
            nb = 0
            for key2 in X:
                try:
                    X[key2] = instructions_dict[key2]
                    nb = nb + instructions_dict[key2]
                except:
                    continue
            X['pollution'] = i
            X['nb_instruction'] = nb
            try:
                model = ske.models.classic.Gumbel(evt, fit_method='mle', ci=0,ci_method=None)
            except:
                continue
            pwcet = model.ppf(1 - 10**-3)
            #print(evt)
            #print(model.ppf(1 - 10**-9))
            #print(max(evt))

            X['pWCET']= int(model.ppf(1 - 10**-3))
            X['ratiopWCET']= model.ppf(1 - 10**-3) / nb
            #X['pWCET'] = max(evt[1:200])
            #X['ratiopWCET']= max(evt[1:200]) / nb
            list_XY.append(X)
    df = pd.DataFrame(list_XY)
    df.to_csv('dataset_pollution-103'+str(0)+'.csv')
    i = i * 2

#df = pd.DataFrame(list_XY)
#df.to_csv('dataset_pollution-all.csv')

#    df.loc[key]['KPSS_test'] = kpss_stat
#    df.loc[key]['p_value_KPSS'] = p_value_KPSS
#    df.loc[key]['BDS_test'] = bds_stat
#    df.loc[key]['p_value_BDS'] = p_value_BDS
#    df.loc[key]['H index'] = H
#    df.loc[key]['C'] = c