import math
import os
import skextremes as ske
import matplotlib.pyplot as plt
import time
import statsmodels.tsa.stattools
import hurst
from pyextremes import EVA, __version__
import numpy as np
import pandas as pd

import pickle
import json

import numpy as np
import pandas as pd
import seaborn as sns

# List all files in a directory using scandir()


i = 1
list_XY = []

#list_XY = []
data = {}
basepath = 'benchmark_txt/'
with os.scandir(basepath) as entries:
    for entry in entries:
        if entry.is_file():
            #print(entry.name)
            data[entry.name] = []
            # Using readlines()
            file1 = open(basepath + entry.name, 'r')
            Lines = file1.readlines()

            count = 0
            # Strips the newline character
            for line in Lines:
                l = line.split(";")
                if 'TIMINGML' in l[0] or 'Module loaded\n' in l[0] or 'rcu:' in l[0] or 'rcu_sched' in l[0] or '(__schedule)' in l[0] or '(schedule)' in l[0] or '(schedule_timeout)' in l[0] or '(rcu_gp_kthread)' in l[0] or 'kthread' in l[0] or 'Exception' in l[0]:
                    continue
                else:
                    try:
                        sample = int(int(''.join(filter(str.isdigit, l[2]))))
                    except:
                        continue
                    data[entry.name].append(sample)


index = [key for key in data]
columns = ['KPSS_test','BDS_test','H index','pWCET','MOET']
df = pd.DataFrame (index=index,columns=columns)
cpt = 0
for key in data:
    evt = data[key]
    # plt.hist(evt)
    print(key)
    """ First test Stationnary"""
    try:
        kpss_stat, p_value_KPSS, lags, crit = statsmodels.tsa.stattools.kpss(evt, regression='c', nlags=None, store=False)
    except:
        print("error kpss ", key)
        print(data[key])
        kpss_stat = -1
        p_value_KPSS = -1

    """ Second test Short indep"""
    try:
        bds_stat, p_value_BDS = statsmodels.tsa.stattools.bds(evt)
    except:
        print("error BDS ", key)
        print(data[key])
        bds_stat = -1
        p_value_BDS = -1

    """ Third test long indep"""
    try:
        H, c, dat = hurst.compute_Hc(evt, kind='price', simplified=True)
    except:
        print("error H index ", key)
        print(data[key])
        H = -1
        c = -1
    a = False
    b = False
    d = False
    if ( p_value_KPSS >= 0.01):
        df.loc[key]['KPSS_test'] = 1
        a = True
    else:
        df.loc[key]['KPSS_test'] = 0
    if ( p_value_BDS  >= 0.01):
        df.loc[key]['BDS_test'] = 1
        b = True
    else:
        df.loc[key]['BDS_test'] = 0
    if (H <= 0.5 + math.exp(-7.51*math.log(math.log(math.log2(len(evt))))+4.58) and
             H >= 0.5 - math.exp(-7.19*math.log(math.log(math.log2(len(evt))))+4.34)):
        df.loc[key]['H index'] = 1
        d = True
        df.loc[key]['BDS_test'] = 1
        b = True
    else :
        df.loc[key]['H index'] = 0
    if (a and b and d):
        cpt = cpt + 1
    model = ske.models.classic.Gumbel(evt, fit_method='mle', ci=0,
                               ci_method=None)

        #pwcet = model.ppf(10**-6)
        #print(evt)
        #print(model.ppf(1 - 10**-9))
        #print(max(evt))
    df.loc[key]['pWCET'] = int(model.ppf(1 - 10**-3))
    df.loc[key]['MOET'] = max(data[key])
#df = pd.DataFrame(list_XY)
df.to_csv('dataset_benchmark_103-.csv')
print(cpt)


#    df.loc[key]['p_value_KPSS'] = p_value_KPSS

#    df.loc[key]['p_value_BDS'] = p_value_BDS

#    df.loc[key]['C'] = c