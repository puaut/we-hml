import pickle
import os
import sys

if len(sys.argv) >= 2:
    source_folder = os.path.expanduser(sys.argv[1])
else:
    source_folder = input("Path of the benchmarks precomputed with save_benchmarks.py: ")

benchmarks = []
benchmark_names = {}
global_instructions = []
global_instructions_indices = {}

with open(source_folder + "/benchmarks.pickle", "rb") as file:
    benchmarks = pickle.load(file)

with open(source_folder + "/benchmark_names.pickle", "rb") as file:
    benchmark_names = pickle.load(file)

with open("/global_instructions.pickle", "rb") as file:
    global_instructions = pickle.load(file)

with open(source_folder + "/global_instructions_indices.pickle", "rb") as file:
    global_instructions_indices = pickle.load(file)

print(source_folder + " loaded!")
print("{} instrs".format(len(global_instructions)))
