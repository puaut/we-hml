import json
import numpy as np


class Benchmark:
    def __init__(self, name, pollution, json_file, result_file):
        self.name = name
        self.pollution = pollution

        with open(json_file) as file:
            self.instructions_dict = json.load(file)
            if ".word" in self.instructions_dict:
                del self.instructions_dict[".word"]
            # print("Lecture json, instrs ",len(self.instructions_dict))

        with open(result_file) as file:
            self.cycles = []
            index = 0
            print(result_file)
            for line in file:
                if line[-1] == "\n":
                    line = line[:-1]
                if "RESULT" in line:
                    assert "INDEX" in line
                    #assert str(index) == line.split("INDEX=")[1].split(";")[0], "{} != {}".format(index, line)
                    index += 1
                    result = line.split("RESULT=")[1]
                    self.cycles.append(int(result))

            self.cycles = np.array(self.cycles)
