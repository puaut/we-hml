import pickle
import sys
import math
import json
import glob
import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

# List of folders (to be updated when changing the location of results/classifiers, etc)
# ---------------------------------------------------------------------------------

# The generated graph is related to a specific cache pollution.
assert len(sys.argv) == 6, "Usage: plot_results_ml.py source_folder json_folder res_folder curves_folder cache_pollution"
source_folder = sys.argv[1]
json_folder = sys.argv[2]
res_folder = sys.argv[3]
curves_folder = sys.argv[4]
pollution = int(sys.argv[5])

#from utils.load_benchmarks import benchmarks, global_instructions

plt.switch_backend('Agg')

print("Pollution: ",pollution)

# Values of features normalized WCET and #Instr in basic blocks
X = []
Y = []
numinstr = []

# Fill in X and Y for all basic blocks
for benchmark in benchmarks:
    if (benchmark.pollution == pollution):
        # X and Y contain the features and WCET respectively
        X.append(benchmark.X)
        Y.append(benchmark.Y)
        numinstr.append(benchmark.total_sum)
print("size X: ",len(X));

# Split data similarly to the splitting done during training
x_train, x_test, y_train, y_test, num_train, num_test = train_test_split(X, Y, numinstr, test_size=0.2, random_state=42)

# Really do the job
os.chdir(source_folder)
for classifier_file in glob.glob("classifier*"+str(pollution)+".pickle"):

    # Arrays to store plots
    plot_X = []
    plot_Y = []
    plot_X_train = []
    plot_Y_train = []
    RE = []
    RE_train = []
    nb_overestimated = 0
    nb_underestimated = 0

    # For each classifier
    print("Classifier: ",classifier_file)

    # Load the classifier
    classifier_path = source_folder + str(classifier_file)
    with open(classifier_path, "rb") as file:
        classifier = pickle.load(file)

    # Obtain the classification for test data
    estimated_wcets = classifier.predict(x_test)

    # For all basic blocks in test data
    for i in range(len(x_test)):
        estimated_wcet = estimated_wcets[i] * num_test[i]
        moet = y_test[i]*num_test[i]
        #if (i%10==0):
        plot_X.append(moet)
        plot_Y.append(estimated_wcet)
        RE.append((float)(estimated_wcet - moet) / float(moet))
                      
        if estimated_wcet >= moet:
            nb_overestimated = nb_overestimated + 1
        else:
            nb_underestimated = nb_underestimated + 1

    # For all basic blocks in training data
    estimated_wcets_train = classifier.predict(x_train)
    for i in range(len(x_train)):
        estimated_wcet = estimated_wcets_train[i] * num_train[i]
        moet = y_train[i]*num_train[i]
        
        # print("WCET estimé pour %s : %s" % (instructions_file, estimated_wcet))
        #if (i%10==0):
        plot_X_train.append(moet)
        plot_Y_train.append(estimated_wcet)
        RE_train.append((float)(estimated_wcet - moet) / float(moet))


    print("Overestimated ",    nb_overestimated);
    print("Underestimated ",    nb_underestimated);
    print("Total ",    nb_overestimated+nb_underestimated);
    print("X.size ",len(plot_X))
    print("Y.size ",len(plot_Y))
    print("Relative error (test): ",np.average(np.absolute(RE)))
    print("Relative error (train): ",np.average(np.absolute(RE_train)))

    fig = plt.figure()
    plt.xlabel("Maximum observed execution time(cycles)")
    plt.ylabel("Estimated WCET (cycles)")
    plt.plot(plot_X, plot_Y, ".", markersize=2)
    #plt.plot(plot_X_train, plot_Y_train, "o", markersize=2)

    max_value = max(max(plot_Y), max(plot_X))
    # plt.axis('equal')
    plt.ylim(0, max_value)
    plt.xlim(0, max_value)

    ident = [0.0, max_value]
    plt.plot(ident,ident)

    # plt.set_aspect('equal', 'box')

    plt.legend()
    plt.show()

    fig_title = os.path.splitext(classifier_file)[0]
    fig_title = fig_title.replace("classifier_", "")
    fig_title = fig_title.replace("_", " - pollution ")

    fig.suptitle(fig_title, fontsize=12)

    fig_name = curves_folder + classifier_file + ".pdf"
    fig.savefig(fig_name)

print("Done")
