import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def r(a,b):
    return (a-b)/a*100
# set width of bar
import math
benchs=[
    "binarysearch",
    "bs",
    "bsort",
    "bsort100",
    "countnegative",
    "crc",
    "expint",
    "fdct",
    "fir",
    "h264_dec",
    "insertsort",
    "jfdctint",
    "matmult",
    "matrix1",
    "ns",
    "nsichneu",
    "petrinet"
    ]
pWCET = np.array([2323,
1088,
361952,
257762,
29581,
66299,
0,
8134,
6595,
425798,
2643,
7414,
335969,
20684,
21950,
23548,
3598
])
WCET= np.array([
2568,
1875,
358380,
257924,
29720,
66867,
6122,
8877,
7646,
426327,
3042,
8070,
336212,
21380,
22018,
26647,
3329

])
    
"""RF = np.array([
r(39783,9953),
r(4685,2918),
r(15306500,4603470),
r(11649841,4297233),
r(971518,131993),
r(3441449,321134),
r(207036,38654),
r(157875,34404),
r(118448,57581),
r(15250961,4002193),
r(92109,16048),
r(200209,41717),
r(9301884,847056),
r(609933,81054),
r(3369,294),
r(655507,323181),
r(1556193,610482),
r(105050,110199)])

NN = np.array([
r(60478,17632),
r(9155,8412),
r(25694314,10906907),
r(23055528,16691142),
r(1188714,113285),
r(5331000,476744),
r(326647,77877),
r(307613,61018),
r(259807,93138),
r(29813632,6354594),
r(216163,21866),
r(297229,61686),
r(13144847,1068555),
r(1040826,138446),
r(7031,549),
r(916445,597819),
r(1283431,1381742),
r(225404,268471)])

GB = np.array([
r(55681,10258),
r(6232,3384),
r(21246242,6041698),
r(15779892,5448030),
r(1297211,138395),
r(4586933,386142),
r(247513,38132),
r(227441,44779),
r(169009,57292),
r(21203900,4620950),
r(126809,17456),
r(294461,54208),
r(13434245,1033901),
r(842101,91832),
r(4440,352),
r(818051,367960),
r(811450,774948),
r(146724,138556)])

BR = np.array([
r(51167,18756),
r(7547,7958),
r(21223010,14076530),
r(19349662,16673793),
r(913016,102326),
r(4395446,367871),
r(286067,86629),
r(239548,51273),
r(217914,127052),
r(25728838,6052465),
r(177145,23251),
r(226887,55211) ,
r(9422152,1008245),
r(825114,129406),
r(6621,512),
r(718853,666865),
r(1070947,1160962),
r(190489,269555)  ])

RIDGE = np.array([
r(57052,19871),
r(8546,8701),
r(24332088,14898469),
r(22378560,17543090),
r(1013038,111748),
r(4990286,381900),
r(314386,86726),
r(273861,56221),
r(247702,132195),
r(28764380,6515148),
r(204842,24989),
r(264237,59561),
r(10393358,1020488),
r(911858,145760),
r(6839,526),
r(802813,682301),
r(1234894,1300361),
r(218671,284721)])
"""
RF = np.array([    7358,
  	 2045,
           3362849 ,
         3170325 ,
    102506,
             277623,
          27704,
             26193,
              40565,
        2941623,
       12293,
        31969,
         735906,
        65679,
              190940,
        427872,
         77039])

NN = np.array([   11728,
    5049   ,
   5251155,
   10866103,
   99415  ,
   329852,
	   35933,
       	   40328 ,
       	   50510,
         3649120,
          15322	  ,
            40103   ,
            780551 ,
            102079 ,
               190940,
            816998 ,
         	   175362 ])

GB = np.array([		      	 7117,
                 2244	,
            4463131  ,
         3847042	,
    	     108291    ,
            	 298225,
           	 28353	   ,
           	 29084	   ,
             	 37570	,
        	 3405644,
       	 12095    ,
         	 35706	    ,
          	 824924    ,
         	 65911	    ,
               	 185426,
           478772,
            92620])

BR = np.array([    12622,
	   5328   ,
    9555110  ,
 	 11216375    ,
    798188      ,
    289192     ,
	   60420       ,
           34523   ,
         82433    ,
      4126177    ,
        16858     ,
    	   38910 ,
     796422     ,
   	   95697   ,
  367042      ,
   699854      ,
	   157400  ])

RIDGE = np.array([    13517,
      5883     ,
     10225058 ,
       11933819,
      87545   ,
             302788  ,
            61358   ,
              37461 ,
               87648,
         4506618  ,
          18584    ,
             41611    ,
            798702   ,
            106144   ,
                  370772   ,
          784958          ,
        167268 ])
"""
RF = np.array([9953,
               2918,
               4603470,
               4297233,
               131993,
               321134,
               38654,
               34404,
               57581,
               4002193,
               16048,
               41717,
               847056,
               81054,
               323181,
               610482,
               110199])

NN = np.array([17632,
               8412,
               10906907,
               16691142,
               113285,
               476744,
               77877,
               61018,
               93138,
               6354594,
               21866,
               61686,
               1068555,
               138446,
               597819,
               1381742,
               268471])

GB = np.array([10258,
               3384,
               6041698,
               5448030,
               138395,
               386142,
               38132,
               44779,
               57292,
               4620950,
               17456,
               54208,
               1033901,
               91832,
               367960,
               774948,
               138556])

BR = np.array([18756,
               7958,
               14076530,
               16673793,
               102326,
               367871,
               86629,
               51273,
               127052,
               6052465,
               23251,
               55211,
               1008245,
               129406,
               666865,
               1160962,
               269555])

RIDGE = np.array([19871,
                  8701,
                  14898469,
                  17543090,
                  111748,
                  381900,
                  86726,
                  56221,
                  132195,
                  6515148,
                  24989,
                  59561,
                  1020488,
                  145760,
                  682301,
                  1300361,
                  284721]) 
"""
MLA =  ["MOET","pWCET","RF", "NN", "GB", "BR", "Ridge"]
barWidth = 1/(len(MLA)+1)


# set height of bar
#bars0 = [ x / x for x in WCET]
bars0 = WCET/WCET
bars1 = pWCET/WCET
bars2 = RF/WCET
bars3 = NN/WCET
bars4 = GB/WCET
bars5 = BR/WCET
bars6 = RIDGE/WCET

# Set position of bar on X axis
r0 = np.arange(len(bars0))

r1 = [x + barWidth for x in r0]
r2 = [x + barWidth for x in r1]
r3 = [x + barWidth for x in r2]
r4 = [x + barWidth for x in r3]
r5 = [x + barWidth for x in r4]
r6 = [x + barWidth for x in r5]

with PdfPages(r'./wcet_ratio103.pdf') as export_pdf:

    # Make the plot
    plt.bar(r0, bars0, color='red', width=barWidth, edgecolor='white', label=MLA[0])
    plt.bar(r1, bars1, color='violet', width=barWidth, edgecolor='white', label=MLA[1])
    plt.bar(r2, bars2, color='#557f2d', width=barWidth, edgecolor='white', label=MLA[2])
    plt.bar(r3, bars3, color='brown', width=barWidth, edgecolor='white', label=MLA[3])
    plt.bar(r4, bars4, color='gray', width=barWidth, edgecolor='white', label=MLA[4])
    plt.bar(r5, bars5, color='blue', width=barWidth, edgecolor='white', label=MLA[5])
    plt.bar(r6, bars6, color='black', width=barWidth, edgecolor='white',label=MLA[6])
    # Add xticks on the middle of the group bars
    plt.xlabel('Benchmarks', fontweight='bold')
    plt.xticks([r + (barWidth *3) for r in range(len(bars0))], benchs, rotation=70)

    # Text on the top of WCET
    for i in range(len(r0)):
        plt.text(x = r0[i]-barWidth*3/3 , y = bars0[i]+0.2, s = WCET[i] , size = 6, rotation=90)

    # Custom the subplot layout
    plt.subplots_adjust(bottom=0.3, top=0.99)

    plt.ylabel("ML Algorithm/pWCET")

    # Create legend & Show graphic
    plt.legend()

    export_pdf.savefig()
    plt.close()

# plt.show()
