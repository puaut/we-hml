from math import ceil
import matplotlib.pyplot as plt
import numpy as np

def num_instructions(instructions_dict):
    return sum((value for _, value in instructions_dict.items()))

#
# Prepare the vector for WCET prediction
# --------------------------------------
# - global instructions: all machine instructions
# - global_instructions_indices: indices of instructions in global vector
# - instruction dict: instructions in basic block
# return: x: percentage of each instruction in basic block

def make_x(global_instructions, global_instructions_indices, instructions_dict):
    total_sum = num_instructions(instructions_dict)

    x = np.zeros(len(global_instructions), dtype=float)
    for instr, count in instructions_dict.items():
        x[global_instructions_indices[instr]] = float(count) / total_sum
    return x, total_sum
