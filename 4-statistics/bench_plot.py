# ---------------------------------------------------------
# Plot the frequency of instructions in a set of json files
# ---------------------------------------------------------

import json
import os
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

def iterate_jsons(source_path, function):
    for path, subdirs, files in os.walk(source_path):
        for file in files:
            file_path = os.path.join(source_path, path, file)
            filename, file_extension = os.path.splitext(file_path)
            if file_extension == ".json" and filename.split("/")[-1] != "test":
                with open(file_path, "r") as file:
                    instrs = json.load(file)
                    function(instrs)


assert len(sys.argv) == 4 , "Usage: tacle_bench_plot.py folder_path folder_with_ref_jsons figname\n" \
                                "Plot instructions in the jsons in a folder (obtained with parser.py)\n" \
                                "Can also use a reference json to remove instructions not in that ref"

source_path = os.path.abspath(sys.argv[1])
ref_path = os.path.abspath(sys.argv[2])
fig_name= os.path.abspath(sys.argv[3])
print("Ref path: ", ref_path)
print("Source path: ", source_path)
print("Figure name", fig_name)

instrs_set = set()
instrs_generated = set()

def add_to_instruction_set(instrs):
    for key in instrs:
        if key != ".word" and key !="ccnt_read":
            instrs_set.add(key)

# Instrs contient le tableau des instructions, argv[2] sert de reference
# Ici on ne stocke que la cle, pas le nombre d'occurrences
iterate_jsons(ref_path, add_to_instruction_set)
   
x = []
y = []

def add_generated(instrs):
    if len(instrs_set) > 0:
        for key in set(instrs):
            if key !=".word":
                if key not in instrs_set and key != "ccnt_read":
                    print("Ignoring instr not in instrs_set in plot",key);
                    del instrs[key]
                instrs_generated.add(key);
    for key in set(instrs):
        if key == ".word" or key == "ccnt_read":
            del instrs[key]
    x.extend(instrs.keys())
    values = np.array(list(instrs.values()))
    y.extend(values / float(np.sum(values)))

iterate_jsons(source_path, add_generated)

print("Instr set :",instrs_set,len(instrs_set))
print("Instr generated :",instrs_generated,len(instrs_generated))

assert len(x) > 0, "No files"

x = np.array(x)
y = np.array(y)

indices = x.argsort()
x = x[indices]
y = y[indices]

#print("x = ",x,len(x));
#print("x = ",x[0],x[1]);

f = plt.figure(figsize=(10, 7))
sns.set(style="whitegrid")
sns.boxplot(x=x, y=y)
#sns.swarmplot(x=x, y=y, color="0", alpha=0.35)

plt.xticks(rotation=70,size=7)
#plt.show()

f.savefig(fig_name)

# Recherche des instructions manquantes dans les benchmarks
for ins in instrs_set:
    if ins not in x:
        print("Instruction manquante ",ins)
