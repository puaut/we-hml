import matplotlib.pyplot as plt
import numpy as np



def r(a,b):
    return (a-b)/a*100


benchs = [
    "binarysearch",
    "bs",
    "bsort",
    "bsort100",
    "countnegative",
    "crc",
    "expint",
    "fdct",
    "fir",
    "h264_dec",
    "insertsort",
    "jfdctint",
    "matmult",
    "matrix1",
    "ns",
    "nsichneu",
    "petrinet"
]


RF = np.array([    7358,
  	 2045,
           3362849 ,
         3170325 ,
    102506,
             277623,
          27704,
             26193,
              40565,
        2941623,
       12293,
        31969,
         735906,
        65679,
              190940,
        427872,
         77039])

NN = np.array([   11728,
    5049   ,
   5251155,
   10866103,
   99415  ,
   329852,
	   35933,
       	   40328 ,
       	   50510,
         3649120,
          15322	  ,
            40103   ,
            780551 ,
            102079 ,
               190940,
            816998 ,
         	   175362 ])

GB = np.array([		      	 7117,
                 2244	,
            4463131  ,
         3847042	,
    	     108291    ,
            	 298225,
           	 28353	   ,
           	 29084	   ,
             	 37570	,
        	 3405644,
       	 12095    ,
         	 35706	    ,
          	 824924    ,
         	 65911	    ,
               	 185426,
           478772,
            92620])

BR = np.array([    12622,
	   5328   ,
    9555110  ,
 	 11216375    ,
    798188      ,
    289192     ,
	   60420       ,
           34523   ,
         82433    ,
      4126177    ,
        16858     ,
    	   38910 ,
     796422     ,
   	   95697   ,
  367042      ,
   699854      ,
	   157400  ])

RIDGE = np.array([    13517,
      5883     ,
     10225058 ,
       11933819,
      87545   ,
             302788  ,
            61358   ,
              37461 ,
               87648,
         4506618  ,
          18584    ,
             41611    ,
            798702   ,
            106144   ,
                  370772   ,
          784958          ,
        167268 ])
""" 
RF = np.array([r(27817,7358),
               r(3280,2045),
               r(10788433,3362849),
               r(8181584,3170325),
               r(676280,102506),
               r(2408245,277623),
               r(140165,27704),
               r(110863,26193),
               r(83268,40565),
               r(10600955,2941623),
               r(65072,12293),
               r(141008,31969),
               r(6566298,735906),
               r(428505,65679),
               r(460787,190940),
               r(407040,427872),
               r(73627,77039)])

NN = np.array([r(39295,11728),
               r(5836,5049),
               r(16875934,5251155),
               r(14799218,10866103),
               r(770631,99415),
               r(3616245,329852),
               r(201422,35933),
               r(196563,40328),
               r(161185,50510),
               r(18635180,3649120),
               r(134807,15322),
               r(198066,40103),
               r(8479746,780551),
               r(651943,102079),
               r(598153,190940),
               r(817744,816998),
               r(143147,175362)])

GB = np.array([r(31950,7117),
               r(3948,2244),
               r(12539694,4463131),
               r(9554490,3847042),
               r(765562,108291),
               r(2742235,298225),
               r(165612,28353),
               r(129335,29084),
               r(108579,37570),
               r(12617797,3405644),
               r(87883,12095),
               r(165749,35706),
               r(7570239,824924),
               r(493099,65911),
               r(499213,185426),
               r(482135,478772),
               r(87733,92620)])

BR = np.array([r(31955,12622),
               r(5594,5328),
               r(13088225,9555110),
               r(11680643,11216375),
               r(588015,798188),
               r(2777390,289192),
               r(171758,60420),
               r(147076,34523),
               r(131459,82433),
               r(15465876,4126177),
               r(106507,16858),
               r(144113,38910),
               r(6095933,796422),
               r(511441,95697),
               r(462742,367042),
               r(641741,699854),
               r(114481,157400)])

RIDGE = np.array([ r(35527,13517),
                  r(5829,5883),
                  r(14950268,10225058),
                  r(13505982,11933819),
                  r(649109,87545),
                  r(3136938,302788),
                  r(188851,61358),
                  r(167395,37461),
                  r(149312,87648),
                  r(17306498,4506618),
                  r(122982,18584),
                  r(166318,41611),
                  r(6676651,798702),
                  r(564456,106144),
                  r(513072,370772),
                  r(740519,784958),
                  r(131498,167268)])"""

for i in range(len(benchs)):
    bench_name = "benchmark_txt/" + benchs[i]

    WCETRF = RF[i]
    WCETNN = NN[i]
    WCETGB = GB[i]
    WCETBR = BR[i]
    WCETR = RIDGE[i]

    times = []
    with open(bench_name + ".txt", "r") as file:
        for line in file.readlines():
            if "RESULT" in line:
                execution_time = int(line.split("=")[3])
                times.append(execution_time)

    print("nb times: ", len(times))


    def num_below(WCET):
        return sum((time < WCET for time in times))


    def distance(WCET):
        return float(abs(max(times) - WCET)) / max(times)


    plt.figure() # figsize=(7, 5)
    plt.gcf().subplots_adjust(left=0.15)
    plt.tight_layout()

    # print("RF below, NN below, GB below, BR below")
    # print("{}\t{}\t{}\t{}\t{}".format(
    #    float(num_below(WCETRF)) / float(len(times) * 100),
    #    float(num_below(WCETNN)) / float(len(times) * 100),
    #    float(num_below(WCETGB)) / float(len(times) * 100),
    #    float(num_below(WCETBR)) / float(len(times) * 100)))

    plt.plot(list(range(len(times))), times, "o", markersize=2, label="Observed")
    plt.axhline(y=WCETRF, color="b", label="RF")
    plt.axhline(y=WCETNN, color="g", label="NN")
    plt.axhline(y=WCETGB, color="r", label="GB")
    plt.axhline(y=WCETBR, color="c", label="BR")
    plt.axhline(y=WCETR, color="m", label="Ridge")
    plt.xlabel("Runs")
    plt.ylabel("Cycles")
    plt.title(benchs[i])
    plt.legend()
    plt.savefig(bench_name + ".pdf")
    plt.show()
