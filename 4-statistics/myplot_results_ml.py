import sys
import os
import pickle
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor, GradientBoostingRegressor
from sklearn.linear_model import LinearRegression, SGDRegressor, LassoLars, ElasticNet, Ridge, BayesianRidge
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.svm import SVR
from sklearn.metrics import *

# Preparing the neural network
network = MLPRegressor(hidden_layer_sizes=(50, 100, 50),
                       # n_iter_no_change=1000000,
                       max_iter=400,
                       activation='relu',
                       solver='adam',
                       alpha=0.001,
                       batch_size='auto',
                       learning_rate='adaptive',
                       learning_rate_init=0.001,
                       power_t=0.5,
                       shuffle=True,
                       random_state=None,
                       tol=0.0001,
                       verbose=False,
                       warm_start=False,
                       momentum=0.9,
                       nesterovs_momentum=True,
                       early_stopping=False,
                       validation_fraction=0.1,
                       beta_1=0.9,
                       beta_2=0.999,
                       epsilon=1e-08)

# param_grid = dict(alpha=10.0 ** -np.arange(2, 4),
#                   activation=['relu', 'logistic'],
#                   hidden_layer_sizes=[(50, 50,), (20, 20, 20,), (100, 100, 100,)])

classifiers = {
    "Random Forest":
        RandomForestRegressor(
            bootstrap=True,
            max_depth=None,
            max_features='auto',
            min_samples_leaf=8,
            min_samples_split=10,
            n_estimators=800,
            n_jobs=4) }
""",
    "Neural Network":
        network,
    "Gradient Boosting regressor":
        GradientBoostingRegressor(
            learning_rate=0.1,
            max_depth=9,
            max_features=13,
            max_leaf_nodes=None,
            min_samples_leaf=60,
            min_samples_split=400,
            min_weight_fraction_leaf=0.0,
            n_estimators=60,
            subsample=0.8),
    "Adaboost":
        AdaBoostRegressor(),
    "Support Vector Machine":
        SVR(gamma='auto'),
    "Bayesian Ridge":
        BayesianRidge(n_iter=400),
    "Linear Regression":
        LinearRegression(
            fit_intercept=True),  # Initially true
    "SGD Regressor":
        SGDRegressor(
            tol=None),
    "LassoLars":
        LassoLars(
            alpha=0.001,
            eps=1,
            positive=True),
    "ElasticNet":
        ElasticNet(
            positive=True,
            fit_intercept=True),
    "Ridge":
        Ridge(alpha=0.02)
}"""

set_pollution = [1, 16, 512]
with open("global_instructions.pickle", "rb") as file:
    glob_instr = pickle.load(file)
for pollution in set_pollution:
    print("#############################################")
    print("Pollution: ", pollution)
    X = []
    Y = []
    numinstr = []
    df = pd.read_csv('dataset_pollution-' + str(pollution) + '.csv')
    for sample in range(1, df.shape[0]):
        line = []
        for key in glob_instr:
            line.append(df[key][sample]/df['nb_instruction'][sample])
        X.append(line)
        Y.append(df['pWCET'][sample]/df['nb_instruction'][sample])
        numinstr.append(df['nb_instruction'][sample])
    print("Loading data done")
    # X = np.array()
    # Y = np.array()
    for name, classifier in classifiers.items():
        x_train, x_test, y_train, y_test,  num_train, num_test = train_test_split(X, Y, numinstr,test_size=0.2, random_state=42)
        classifier.fit(x_train, y_train)
        y_predict = classifier.predict(x_test)
        res = [(y_predict[i] - y_test[i]) / y_test[i] for i in range(len(y_test))]
        moet_rel = np.mean(res)

        # Arrays to store plots
        plot_X = []
        plot_Y = []


        # For each classifier
        print("Classifier: ", classifier)

        # Load the classifier

        # Obtain the classification for test data
        estimated_wcets = classifier.predict(x_test)

        # For all basic blocks in test data
        for i in range(len(x_test)):
            estimated_wcet = estimated_wcets[i] * num_test[i]
            moet = y_test[i] * num_test[i]
            # if (i%10==0):
            plot_X.append(moet)
            plot_Y.append(estimated_wcet)


        fig = plt.figure()
        plt.xlabel("Maximum observed execution time(cycles)")
        plt.ylabel("Estimated WCET (cycles)")
        max_value = min(max(plot_Y), max(plot_X))

        plt.plot(plot_X, plot_Y, ".", markersize=2)
        # plt.plot(plot_X_train, plot_Y_train, "o", markersize=2)

        # plt.axis('equal')
        plt.ylim(0, max_value)
        plt.xlim(0, max_value)

        ident = [0.0, max_value]
        plt.plot(ident, ident)

        # plt.set_aspect('equal', 'box')

        plt.legend()
        plt.show()
        classifier_file = "classifier_"+name+"_"+str(pollution)
        fig_title = os.path.splitext(classifier_file)[0]
        fig_title = fig_title.replace("classifier_", "")
        fig_title = fig_title.replace("_", " - pollution ")

        fig.suptitle(fig_title, fontsize=18,y=0.94)

        fig_name = classifier_file + ".pdf"
        fig.savefig(fig_name)