import sys
import os
import pickle

import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor, GradientBoostingRegressor
from sklearn.linear_model import LinearRegression, SGDRegressor, LassoLars, ElasticNet, Ridge, BayesianRidge
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.svm import SVR
from sklearn.metrics import *


# Preparing the neural network
network = MLPRegressor(hidden_layer_sizes=(50, 100, 50),
                       # n_iter_no_change=1000000,
                       max_iter=800,
                       activation='relu',
                       solver='adam',
                       alpha=0.001,
                       batch_size='auto',
                       learning_rate='adaptive',
                       learning_rate_init=0.001,
                       power_t=0.5,
                       shuffle=True,
                       random_state=None,
                       tol=0.0001,
                       verbose=False,
                       warm_start=False,
                       momentum=0.9,
                       nesterovs_momentum=True,
                       early_stopping=False,
                       validation_fraction=0.1,
                       beta_1=0.9,
                       beta_2=0.999,
                       epsilon=1e-08)

# param_grid = dict(alpha=10.0 ** -np.arange(2, 4),
#                   activation=['relu', 'logistic'],
#                   hidden_layer_sizes=[(50, 50,), (20, 20, 20,), (100, 100, 100,)])

classifiers = {
    "Random Forest":
        RandomForestRegressor(
            bootstrap=True,
            max_depth=None,
            max_features='auto',
            min_samples_leaf=8,
            min_samples_split=10,
            n_estimators=800,
            n_jobs=4),
    "Neural Network":
        network,
    "Gradient Boosting regressor":
        GradientBoostingRegressor(
            learning_rate=0.1,
            max_depth=9,
            max_features=13,
            max_leaf_nodes=None,
            min_samples_leaf=60,
            min_samples_split=400,
            min_weight_fraction_leaf=0.0,
            n_estimators=60,
            subsample=0.8),
    "Adaboost":
        AdaBoostRegressor(),
    "Support Vector Machine":
        SVR(gamma='auto'),
    "Bayesian Ridge":
        BayesianRidge(n_iter=400),
    "Linear Regression":
        LinearRegression(
            fit_intercept=True),  # Initially true
    "SGD Regressor":
        SGDRegressor(
            tol=None),
    "LassoLars":
        LassoLars(
            alpha=0.001,
            eps=1,
            positive=True),
    "ElasticNet":
        ElasticNet(
            positive=True,
            fit_intercept=True),
    "Ridge":
        Ridge(alpha=0.02)
}

set_pollution = [1,2,4,8,16,32,64,128,256,512]
with open("global_instructions.pickle", "rb") as file:
    glob_instr = pickle.load(file)

lignex= {}
lignex["regressor"]= "name"
lignex["pollution"]= 0
lignex["r2_score"]= 0
lignex["max_error"]= 0
lignex["mean_absolute_error"]= 0
lignex["mean_relative_MOET"]= 0
lignex["exp_var"]= 0
list_scores = []
for pollution in set_pollution:
    print("#############################################")
    print("Pollution: ", pollution)
    X = []
    Y = []
    df = pd.read_csv('dataset_pollution-'+str(pollution)+'.csv')
    for sample in range(1,df.shape[0]):
        line = []
        for key in glob_instr:
            line.append(df[key][sample]/df['nb_instruction'][sample])
        X.append(line)
        Y.append(df['pWCET'][sample]/df['nb_instruction'][sample])
    print("Loading data done")
    # X = np.array()
    # Y = np.array()
    for name, classifier in classifiers.items():
        x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=42)

        classifier.fit(x_train, y_train)
        r2 = classifier.score(x_test, y_test)
        y_predict = classifier.predict(x_test)
        r2_diff = r2_score(y_test,y_predict)
        max_err = max_error(y_test,y_predict)
        mean_relative_err = mean_absolute_error(y_test,y_predict)
        res = [(y_predict[i] - y_test[i])/ y_test[i] for i in range(len(y_test))]
        moet = np.mean(res)
        exp_var = explained_variance_score(y_test,y_predict)
        lignex["regressor"]= name
        lignex["pollution"]= pollution
        lignex["r2_score"]= r2
        lignex["max_error"]= max_err
        lignex["mean_absolute_error"]= mean_relative_err
        lignex["mean_relative_MOET"]= moet
        lignex["exp_var"]= exp_var
        list_scores.append(lignex)
        print("Method: " + name + " Score r2: " + str(r2))
        print("Method: " + name + " Score max_err: " + str(max_err))
        print("Method: " + name + " Score mean_abs_err: " + str(mean_relative_err))
        print("Method: " + name + " Score moet: " + str(moet))
        print("Method: " + name + " Score exp_var: " + str(exp_var))
        # print_stats(classifier, verbose=False)
        path = "classifier_" + name + "_" + format(pollution) + ".pickle"
        with open(path, "wb") as file:
            pickle.dump(classifier, file)

dd = pd.DataFrame(list_scores)
dd.to_csv("scores103.csv")
